#ifndef TAGDB_HPP
#define TAGDB_HPP

#include <stddef.h>

namespace godot {

class String;

namespace _TagDB {

template<typename TypeT>
void register_type()
{
	register_type(TypeT::___get_type_name(), TypeT::___get_id(), TypeT::___get_base_id());
}

void register_type(const char* name, size_t type_tag, size_t base_type_tag);
bool is_type_known(size_t type_tag);
void register_global_type(const char *name, size_t type_tag, size_t base_type_tag);
bool is_type_compatible(size_t type_tag, size_t base_type_tag);

void set_type_tag_name(const String& name, size_t type_tag);
size_t get_type_tag_name(const String& name);

} // namespace _TagDB

} // namespace godot

#endif // TAGDB_HPP
