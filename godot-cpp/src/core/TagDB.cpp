#include "TagDB.hpp"

#include <unordered_map>
#include <map>
#include <assert.h>

#include <GodotGlobal.hpp>

#include <String.hpp>

namespace godot {

namespace _TagDB {

std::unordered_map<size_t, size_t>& get_parent_to()
{
	static std::unordered_map<size_t, size_t> map;
	return map;
}
std::map<godot::String, size_t> name_map;

void register_type(const char* name, size_t type_tag, size_t base_type_tag) 
{
	set_type_tag_name(name, type_tag);

	if (type_tag != base_type_tag) 
	{
		assert((get_parent_to().find(type_tag) == get_parent_to().end() && "Type is allready registered!"));
		get_parent_to()[type_tag] = base_type_tag;
	}
}

bool is_type_known(size_t type_tag) {
	return get_parent_to().find(type_tag) != get_parent_to().end();
}

void register_global_type(const char *name, size_t type_tag, size_t base_type_tag) {

	godot::nativescript_1_1_api->godot_nativescript_set_global_type_tag(godot::_RegisterState::language_index, name, (const void *)type_tag);

	register_type(name, type_tag, base_type_tag);
}

bool is_type_compatible(size_t ask_tag, size_t have_tag) {

	if (have_tag == 0)
		return false;

	size_t tag = have_tag;

	while (tag != 0) {
		if (tag == ask_tag)
			return true;

		tag = get_parent_to()[tag];
	}

	return false;
}

void set_type_tag_name(const godot::String& name, size_t type_tag)
{
	name_map[name] = type_tag;
}

size_t get_type_tag_name(const godot::String& name)
{
	std::map<godot::String, size_t>::const_iterator it = name_map.find(name);
	return it != name_map.cend() ? it->second : 0;
}

} // namespace _TagDB

} // namespace godot
