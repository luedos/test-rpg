### Project **test-rpj**

#### Description

Simple project to test abilities of Godot as game engine, and gdnative specifically.

#### TODO:
- Level editor
- Better character
	- Movement FSM
	- Simple animation based on movement
- Better animation of room transitioning
	- Beter transition menu animations
	- Room transition based on characted movement
- Better menu animations overall
- Save mechanism
- Enemies
- ...

#### Already in:
- Room loading handling
	- Async room loader
	- RoomMgr
- Game State FSM
- Menu mechanism
	- Main menu logic
	- Menu stack
	- Menu animation states
- Sequences
	- Sequence stack
	- Different sequences (like room changing, game loading)