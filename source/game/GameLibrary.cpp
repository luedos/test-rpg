#include "pch.h"

#include "level/actors/Character.h"

#include "level/rooms/Room.h"
#include "level/rooms/RoomExit.h"

#include "game_states/GameState.h"
#include "game_states/GameplayState.h"
#include "game_states/LoadingState.h"

#include "interface/menus/Menu.h"
#include "interface/menus/transitions/MenuTransitionNode.h"
#include "interface/menus/transitions/AnimationMenuTransition.h"

#include "global/GameMgr.h"
#include "global/RoomMgr.h"
#include "global/SaveMgr.h"


extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) 
{
	godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) 
{
	godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) 
{
	godot::Godot::nativescript_init(handle);

	//
	// Managers registration
	//
	godot::register_class<bwn::GameMgr>();
	godot::register_class<bwn::RoomMgr>();
	godot::register_class<bwn::SaveMgr>();

	//
	// UI registration.
	//
	godot::register_class<bwn::Menu>();
	godot::_TagDB::register_type<bwn::MenuTransitionNode>(); // we only registering this for our own usage, we will not provide this class for creation.
	godot::register_class<bwn::AnimationMenuTransition>();

	//
	// Game states registration.
	//
	godot::register_class<bwn::GameState>();
	godot::register_class<bwn::GameplayState>();
	godot::register_class<bwn::LoadingState>();

	// 
	// Actors registration.
	//
	godot::register_class<bwn::Character>();

	//
	// Level staff registration.
	//
	godot::register_class<bwn::Room>();
	godot::register_class<bwn::RoomExit>();

}
