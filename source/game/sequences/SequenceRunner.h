#pragma once

#include "Sequence.h"

namespace bwn
{

class SequenceRunner
{
	//
	// Public aliases.
	//
public:
	using SequencePtr = std::unique_ptr<Sequence>;
	
	//
	// Private alliasing.
	//
private:
	struct SequenceObj
	{
		SequenceObj(SequencePtr&& ptr, bool isPaused);
		SequenceObj(SequenceObj&&) noexcept;
		SequenceObj& operator=(SequenceObj&&) noexcept;

		SequencePtr seq;
		bool paused;
	};
	using SequenceContainer = std::vector<SequenceObj>;

	//
	// Construction and destruction.
	//
public:
	// Simple constructor.
	SequenceRunner();
	// Simple destructor.
	~SequenceRunner();

	//
	// Public interface.
	//
public:
	// Update sequences.
	void UpdateSequences(real_t delta);
	// Start the sequence runner (started by default).
	void StartSequences();
	// Pauses the seqeunce runner.
	void PauseSequences();
	// Returns true if no sequnces is exist.
	inline bool IsSequencesEmpty() const { return m_container.empty(); }
	// Returns true in no sequencesis runing at the moment (due to abcence os sequences or general pause).
	inline bool IsSequencesRuning() const { return !IsSequencesEmpty() && !IsSequencesPaused(); }
	// Returns true if runner is paused.
	bool IsSequencesPaused() const { return m_paused; }
	// Returns true if currently running sequence is unique mode.
	bool IsCurrentSequenceExclusive() const;
	// Adds Sequence to the pool. Sequence position determined by the priority and RunMode, but if both of these criteriase are equal to some other sequence, general order will be FIFO.
	void AddSequnce(SequencePtr sequence, bool replaceIfUnique = true);
	// Clears all sequences.
	void ClearSequences();
	// Removes sequence by specific key (if there is more sequences with same key, all of them will be removed). Returns number of removed sequences.
	std::size_t RemoveSequence(const Sequence::Key& key);

	//
	// Private methods.
	//
private:
	// Removes sequences, but without refreshing states,
	std::size_t RemoveSequenceRaw(const Sequence::Key& key);
	// Refreshes sequences pause/start states, accordingly to m_paused.
	void RefreshSequences();


	//
	// Private members.
	//
private:
	SequenceContainer m_container;
	bool m_paused;
};

} // namespace bwn.