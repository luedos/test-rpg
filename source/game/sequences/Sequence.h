#pragma once

namespace bwn
{

class SequenceRunner;

class Sequence
{
	friend SequenceRunner;

	//
	// Public enums.
	//
public:
	enum ERunMode
	{
		DEFAULT_MODE    = 0,
		UNIQUE_MODE     = 1 << 0,	// If this bit is active, there is can be only one sequence of the same key at the time
		EXCLUSIVE_MODE  = 1 << 1	// If this bit is active, sequence will be ran exclusively, while all other sequences will be paused.
	};

	//
	// Public aliasing.
	//
public:
	using Key = std::size_t;

	//
	// Constunts.
	//
public:
	static const uint32_t k_minPrio = 0;
	static const uint32_t k_lowPrio = 100;
	static const uint32_t k_midPrio = 200;
	static const uint32_t k_highPrio = 300;

	//
	// Constraction and destruction.
	//
public:
	// Default constructor.
	Sequence(uint32_t priority, ERunMode mode)
		: m_priority{priority}
		, m_mode{mode}
		, m_inited{false}
	{}
	// Default destructor.
	virtual ~Sequence() = default;

	//
	// Public interface.
	//
public:
	// Returns priority of the sequence.
	inline uint32_t GetPriority() const { return m_priority; };
	// Returns run mode of the sequence.	
	inline ERunMode GetMode() const { return m_mode; };
	// Returns true if sequence must be ran exclusively.
	inline bool IsExclusive() const { return (m_mode & ERunMode::EXCLUSIVE_MODE) != ERunMode::DEFAULT_MODE; }
	// Returns true if sequence must be unique.
	inline bool IsUnique() const { return (m_mode & ERunMode::UNIQUE_MODE) != ERunMode::DEFAULT_MODE; }
	// Returns true if sequence was initialized (Push method was called).
	inline bool IsInitialized() const { return m_inited; }
	// Returns true, if sequence counts as less of the priority then other sequence.
	// If priority is equal returns value depending on sequences mode by scale:
	// DEFAULT_MODE < UNIQUE_MODE < EXCLUSIVE_MODE < UNIQUE_MODE && EXCLUSIVE_MODE
	inline bool IsLess(const Sequence& other) const 
	{ 
		return m_priority != other.m_priority 
			? m_priority < other.m_priority
			: m_mode < other.m_mode; 
	} 
	// Returns true if both sequences is equal in priorities, and modes.
	inline bool IsEqual(const Sequence& other)
	{
		return m_mode == other.m_mode && m_priority == other.m_priority;
	}
	// Returns unique key connected by the sequence. Used by the unique and parallelUnique seqeuences. 
	virtual Key GetKey() const = 0;

	//
	// Private interface called from SequenceRunner.
	//
private:
	// Actual update method of the sequence (returns true if sequence is finished).
	virtual bool Update(real_t delta) = 0;
	// Called on the sequence start.
	void Start()
	{
		if (!m_inited)
		{
			m_inited = true;
			OnInit();
		}
		OnStartImpl();
	}
	// Called on sequence pause.
	void Pause()
	{
		OnPauseImpl();
	}
	// calleback on sequence being started.
	virtual void OnStartImpl() = 0;
	// calleback on sequence being paused.
	virtual void OnPauseImpl() = 0;
	// Allways called on first start method.
	virtual void OnInit() = 0;

	//
	// Private members.
	//
private:
	// Priority of the sequence.
	uint32_t m_priority;
	// Run mode of the sequence.
	ERunMode m_mode;
	// Is sequence was initialized.
	bool m_inited;
};

} // namespace bwn

