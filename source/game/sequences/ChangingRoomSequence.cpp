#include "pch.h"

#include "ChangingRoomSequence.h"

#include "global/RoomMgr.h"

#include "game_states/GameplayState.h"

#include "interface/menus/Menu.h"

#include "level/rooms/RoomHandler.h"
#include "level/rooms/Room.h"
#include "level/actors/Character.h"

using namespace bwn;

ChangingRoomSequence::ChangingRoomSequence(GameplayState* state, const godot::String& roomId, const godot::String& exitPath)
	: Sequence{ Sequence::k_highPrio, ERunMode(Sequence::UNIQUE_MODE | Sequence::EXCLUSIVE_MODE) }
	, m_gameplay{ state }
	, m_transitionMenu{ nullptr }
	, m_roomId{ roomId }
	, m_exitPath{ exitPath }
	, m_currentStage{ EStage::MENU_OPENNING }
{

}

ChangingRoomSequence::~ChangingRoomSequence()
{
	if (!IsInitialized()) {
		return;
	}

	GameplayState*const state = GameplayState::GetInstance();
	if (state == nullptr) {
		return;
	}

	Menu*const transition = m_transitionMenu.GetObject();
	if (transition != nullptr && state->GetTransition() == transition){
		state->PopTransition();
	}
}

ChangingRoomSequence& ChangingRoomSequence::SetGameplayState(GameplayState* gameplay)
{
	if (!IsInitialized()) {
		m_gameplay = gameplay;
	}

	return *this;
}

ChangingRoomSequence& ChangingRoomSequence::SetRoomId(const godot::String& roomId)
{
	m_roomId = roomId;

	if (IsInitialized()) {
		RoomMgr::GetInstance()->LoadRoom(m_roomId);
	}

	return *this;
}

ChangingRoomSequence& ChangingRoomSequence::SetExitObjectPath(const godot::String& exitPath)
{
	m_exitPath = exitPath;
	return *this;
}

ChangingRoomSequence& ChangingRoomSequence::SetTransitionMenu(Menu* menu)
{
	BWN_ASSERT_WITH_MSG(!IsInitialized(), "Setting transition menu after sequence was initialized will not do anything.");
	if (!IsInitialized()) {
		m_transitionMenu = menu;
	}
	return *this;
}

Sequence::Key ChangingRoomSequence::GetKey() const 
{
	return typeid(ChangingRoomSequence).hash_code();
}

bool ChangingRoomSequence::Update(real_t delta)
{
	GameplayState*const state = m_gameplay.GetObject();

	BWN_ASSERT_WITH_MSG(state != nullptr && state == GameplayState::GetInstance(), "Gameplay state is removed or changed in the middle of the room changing!");
	if (state == nullptr || state != GameplayState::GetInstance()) {
		return true;
	}

	// Just double check if we in the right transition or not.
	{
		Menu*const stateMenu = state->GetTransition();
		Menu*const myMenu = m_transitionMenu.GetObject();
		BWN_ASSERT_WITH_MSG(stateMenu == myMenu || myMenu == nullptr, "Transition menu was changed in the middle of the room changing.");
		if (stateMenu != myMenu && myMenu != nullptr) {
			m_transitionMenu = nullptr;
		}
	}

	switch(m_currentStage)
	{
	case EStage::MENU_OPENNING:
	{
		Menu*const menu = m_transitionMenu.GetObject();
		if (menu == nullptr || menu->IsFullyOpened()) {
			m_currentStage = EStage::ROOM_LOADING;	
		}

		return false;
	}

	case EStage::ROOM_LOADING:
	{
		RoomMgr*const mgr = RoomMgr::GetInstance();
		const bool roomKnown = mgr->IsRoomKnown(m_roomId);
		BWN_ASSERT_WITH_MSG(roomKnown, "Failed to load room \"" << m_roomId.ascii().get_data() << "\"!");
		if (!roomKnown)
		{
			m_currentStage = EStage::EXIT;
			return false;
		}

		if (!mgr->IsRoomLoaded(m_roomId)) {
			return false;
		}

		RoomMgr::RoomHandlerPtr room = mgr->GetRoomHandler(m_roomId);
		const bool roomLoaded = room->IsValid();
		BWN_ASSERT_WITH_MSG(roomLoaded, "Something gone wrong, can't load the room \"" << m_roomId.ascii().get_data() <<  "\".");
		if (!roomLoaded)
		{
			m_currentStage = EStage::EXIT;
			return false;
		}

		const bool roomSeted = state->SetRoom(room);
		BWN_ASSERT_WITH_MSG(roomSeted, "Something gone wrong, can't load the room \"" << m_roomId.ascii().get_data() <<  "\".");
		if (!roomSeted)
		{
			m_currentStage = EStage::EXIT;
			return false;
		}

		Character*const character = state->GetMainCharacter();
		BWN_ASSERT_WITH_MSG(character != nullptr, "Main character is missing while loading new room \"" << m_roomId.ascii().get_data() <<  "\".");
		if (character == nullptr)
		{
			m_currentStage = EStage::EXIT;
			return false;
		}

		// I don't really like making exit as node2d, by node itself does't has any position.
		// Mb, I just not understanding something, and regular node is not used in any way to define world position of an object...
		godot::Node2D*const exit = room->GetRoom()->get_node<godot::Node2D>(godot::NodePath(m_exitPath));
		BWN_ASSERT_WITH_MSG(exit != nullptr, "Can't find correct spot to place character in, while loading room \"" << m_roomId.ascii().get_data() <<  "\". Desired spot name is \"" << m_exitPath.ascii().get_data() << "\"");
		if (exit == nullptr)
		{
			m_currentStage = EStage::EXIT;
			return false;
		}

		character->set_position(exit->get_position());

		m_currentStage = EStage::EXIT;
		return false;
	}

	case EStage::EXIT:	
		return true;

	default:
		BWN_ASSERT_WITH_MSG(false, "Something in room loading sequence gone wrong, incorrect stage reached.");
		return true;
	}


}

void ChangingRoomSequence::OnStartImpl()
{}

void ChangingRoomSequence::OnPauseImpl()
{}

void ChangingRoomSequence::OnInit()
{
	GameplayState*const state = m_gameplay.GetObject();
	BWN_ASSERT_WITH_MSG(state != nullptr, "Room changing sequence is pushed when gameplay is not initialized.");
	RoomMgr::GetInstance()->LoadRoom(m_roomId);
	if (state != nullptr)
	{
		Menu* menu = m_transitionMenu.GetObject();
		if (menu == nullptr) {
			menu = state->GetRoomTransitionMenu();
		} 

		if (!state->PushTransition(menu)) {
			menu = nullptr;
		}
		
		m_transitionMenu = menu;
	}
}
