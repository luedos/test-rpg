#pragma once

#include "Sequence.h"

namespace bwn
{

class GameplayState;
class Menu;

class ChangingRoomSequence : public Sequence
{
	//
	// Private classes.
	//
private:
	enum class EStage
	{
		MENU_OPENNING,
		ROOM_LOADING,
		EXIT
	};

	//
	// Construction and destruction.
	//
public:
	ChangingRoomSequence(GameplayState* gameplay, const godot::String& roomId, const godot::String& exitPath);
	~ChangingRoomSequence();

	//
	// Public interface.
	//
public:
	// Sets gameplay state to be used. This is works only before pushing sequence.
	ChangingRoomSequence& SetGameplayState(GameplayState* gameplay);
	// Sets room id, to load.
	ChangingRoomSequence& SetRoomId(const godot::String& roomId);
	// Sets object for exit.
	ChangingRoomSequence& SetExitObjectPath(const godot::String& exitPath);
	// Sets transition menu, if none set will be used default one from gameplay state.
	ChangingRoomSequence& SetTransitionMenu(Menu* menu);
	// Returns unique key connected by the sequence. Used by the unique and parallelUnique seqeuences. 
	virtual Key GetKey() const override;

	//
	// Private methods.
	//
private:
	// Actual update method of the sequence (returns true if sequence is finished).
	virtual bool Update(real_t delta) override;
	// calleback on sequence being started.
	virtual void OnStartImpl() override;
	// calleback on sequence being paused.
	virtual void OnPauseImpl() override;
	// Allways called on first start method.
	virtual void OnInit() override;

	//
	// Private members.
	//
private:
	// Gameplay state, sequece were pushed in.
	ObjectHolder<GameplayState> m_gameplay;
	// Menu to be used as a transition.
	ObjectHolder<Menu> m_transitionMenu;
	// Room id to be transported into. 
	godot::String m_roomId;
	// Path for the object, which is used as exit point.
	godot::String m_exitPath;
	// Current stage of the transition.
	EStage m_currentStage;
};

} // namespace bwn.