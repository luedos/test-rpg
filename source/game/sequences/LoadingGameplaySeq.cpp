#include "pch.h"

#include "LoadingGameplaySeq.h"
#include "game_states/GameplayState.h"
#include "global/RoomMgr.h"
#include "level/rooms/RoomHandler.h"

using namespace bwn;

const char*const LoadingGameplaySeq::k_seqName = "LoadingGameplaySeq";

LoadingGameplaySeq::LoadingGameplaySeq(shared_obj_ptr<GameplayState> state, uint32_t priority)
	: Sequence{ priority, ERunMode::UNIQUE_MODE }
	, m_state{ std::move(state) }
{}

void LoadingGameplaySeq::SetRoomIds(std::vector<godot::String> ids)
{
	m_roomIds = std::move(ids);

	if (IsInitialized())
	{
		RefreshRoomsInMgr();
	}
}

void LoadingGameplaySeq::RefreshRoomsInMgr() const
{
	RoomMgr::GetInstance()->RefreshRoomList(m_roomIds);
}

bool LoadingGameplaySeq::Update(real_t delta)
{
	BWN_ASSERT_WITH_MSG(m_state.is_valid(), "Invalid gameplay state for the loading sequence.");
	if (!m_state.is_valid()) 
	{
		return true;
	}

	RoomMgr*const roomMgr = RoomMgr::GetInstance();

	bool success = true;

	for (std::vector<godot::String>::const_iterator idIt = m_roomIds.cbegin(); idIt != m_roomIds.end();)
	{
		const bool isKnown = roomMgr->IsRoomKnown(*idIt);
		BWN_ASSERT_WITH_MSG(isKnown, "Failed to load room \"" << idIt->ascii().get_data() << "\".");
		if (!isKnown)
		{
			idIt = m_roomIds.erase(idIt);
			continue;
		}

		if (!roomMgr->IsRoomLoaded(*idIt))
		{
			success = false;
			break;
		}

		++idIt;
	}
	
	if (success)
	{
		for (const godot::String& roomId : m_roomIds)
		{
			const RoomMgr::RoomHandlerPtr handler = roomMgr->GetRoomHandler(roomId);
			if (handler)
			{
				m_state.get_object()->AddRoom(handler);
			} 
		}	
	}

	return success;
}

void LoadingGameplaySeq::OnStartImpl()
{}

void LoadingGameplaySeq::OnPauseImpl()
{}

void LoadingGameplaySeq::OnInit()
{
	RefreshRoomsInMgr();
}

Sequence::Key LoadingGameplaySeq::GetKey() const
{
	// The easiest way of making this key unique.
	return reinterpret_cast<Sequence::Key>(k_seqName);
}
