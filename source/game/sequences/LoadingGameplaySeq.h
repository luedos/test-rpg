#pragma once

#include "Sequence.h"

namespace bwn
{

class GameplayState;

class LoadingGameplaySeq : public Sequence
{
	//
	// Public constants.
	// 
public:
	static const char*const k_seqName;

	//
	// Construction and destruction.
	//
public:
	LoadingGameplaySeq(shared_obj_ptr<GameplayState> state, uint32_t priority = Sequence::k_midPrio);

	//
	// Public interface.
	//
public:
	// Sets ids of rooms which would be loaded for gameplay.
	void SetRoomIds(std::vector<godot::String> ids);

	//
	// Private methods.
	//
private:
	// Refreshes rooms in room loader based on the m_roomIds and m_mainRoomId.
	void RefreshRoomsInMgr() const;

	//
	// Private Sequence methods.
	//
private:
	// Actual update method of the sequence (returns true if sequence is finished).
	virtual bool Update(real_t delta) override;
	// Called on the sequence start.
	virtual void OnStartImpl() override;
	// Called on sequence pause.
	virtual void OnPauseImpl() override;
	// Implementation of the init method.
	virtual void OnInit() override;	
	// Returns unique key connected by the sequence. Used by the unique and parallelUnique seqeuences. 
	virtual Key GetKey() const override;

	//
	// Private members.
	//
private:
	shared_obj_ptr<GameplayState> m_state;
	std::vector<godot::String> m_roomIds;
};

} // namespace bwn
