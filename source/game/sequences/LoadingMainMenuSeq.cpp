#include "pch.h"

#include "LoadingMainMenuSeq.h"

using namespace bwn;

const char*const LoadingMainMenuSeq::k_seqName = "LoadingMainMenuSeq";

LoadingMainMenuSeq::LoadingMainMenuSeq(uint32_t priority)
	: Sequence{ priority, ERunMode::UNIQUE_MODE }
{}

bool LoadingMainMenuSeq::Update(real_t delta)
{
	// Will perform saving of the progress in the future.

	return true;
}

void LoadingMainMenuSeq::OnStartImpl()
{}

void LoadingMainMenuSeq::OnPauseImpl()
{}

void LoadingMainMenuSeq::OnInit()
{
	// Will perform saving of the progress in the future.
}

Sequence::Key LoadingMainMenuSeq::GetKey() const
{
	// The easiest way of making this key unique.
	return reinterpret_cast<Sequence::Key>(k_seqName);
}