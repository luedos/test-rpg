#include "pch.h"

#include "SequenceRunner.h"

#include "Sequence.h"

using namespace bwn;

SequenceRunner::SequenceObj::SequenceObj(SequencePtr&& ptr, bool isPaused)
	: seq{ std::move(ptr) }
	, paused{ isPaused }
{}

SequenceRunner::SequenceObj::SequenceObj(SequenceObj&&) noexcept = default;
SequenceRunner::SequenceObj& SequenceRunner::SequenceObj::operator=(SequenceObj&&) noexcept= default;

SequenceRunner::SequenceRunner()
	: m_container{}
	, m_paused{ false }
{}

SequenceRunner::~SequenceRunner()
{
	// Just in case if logic will change from simple container cleaning.
	ClearSequences();
}

void SequenceRunner::UpdateSequences(real_t delta)
{
	if (IsSequencesEmpty() || IsSequencesPaused()) {
		return;
	}

	const bool exclusiveMode = m_container.back().seq->IsExclusive();

	for (SequenceContainer::reverse_iterator seqIt = m_container.rbegin(); 
		seqIt != m_container.rend() && seqIt->seq->IsExclusive() == exclusiveMode;)
	{
		if (seqIt->seq->Update(delta))
		{
			seqIt = SequenceContainer::reverse_iterator(m_container.erase(--(seqIt.base())));
		}
		else
		{
			++seqIt;
		}
	}
	
}

void SequenceRunner::StartSequences()
{
	if (m_paused)
	{
		m_paused = false;
		RefreshSequences();
	}
}

void SequenceRunner::PauseSequences()
{
	if (!m_paused)
	{
		m_paused = true;
		RefreshSequences();
	}
}

bool SequenceRunner::IsCurrentSequenceExclusive() const
{
	return !m_container.empty() && m_container.front().seq->IsExclusive();
}

void SequenceRunner::AddSequnce(SequencePtr sequence, bool replaceIfUnique)
{
	BWN_ASSERT_WITH_MSG(sequence, "Tried to push invalid sequence.");
	if (!sequence) {
		return;
	}

	if (m_container.empty())
	{
		if (!m_paused)
		{
			sequence->Start();
		}
		m_container.emplace_back(std::move(sequence), m_paused);
		return;
	}

	if (sequence->IsUnique())
	{	
		if (replaceIfUnique)
		{
			RemoveSequence(sequence->GetKey());
		}
		else
		{
			const Sequence::Key key = sequence->GetKey();
			const SequenceContainer::iterator seqIt = std::find_if(
				m_container.begin(), 
				m_container.end(), 
				[&key](const SequenceObj& seq) -> bool
				{
					return seq.seq->GetKey() == key;
				});

			if (seqIt != m_container.end())
			{
				return;
			}
		}
	}
	const bool hasExclusive = m_container.back().seq->IsExclusive();
	const bool isFirst = m_container.back().seq->IsLess(*sequence);
	const bool paused = m_paused || (hasExclusive && !isFirst);

	if (!paused)
	{
		sequence->Start();
	}


	SequenceContainer::const_iterator seqIt = std::find_if(
		m_container.cbegin(),
		m_container.cend(),
		[&sequence](const SequenceObj& r)->bool
	{
		return !r.seq->IsLess(*sequence);
	});

	m_container.insert(seqIt, SequenceObj{ std::move(sequence), paused });

	RefreshSequences();
}

void SequenceRunner::ClearSequences()
{
	m_container.clear();
}

std::size_t SequenceRunner::RemoveSequence(const Sequence::Key& key)
{
	const std::size_t ret = RemoveSequenceRaw(key);
	RefreshSequences();
	return ret;
}

std::size_t SequenceRunner::RemoveSequenceRaw(const Sequence::Key& key)
{
	const SequenceContainer::const_iterator seqIt = std::remove_if(
		m_container.begin(), 
		m_container.end(), 
		[&key](const SequenceObj& seq)->bool
		{
			return seq.seq->GetKey() == key;
		});

	const std::size_t count = std::distance(seqIt, m_container.cend());
	m_container.erase(seqIt, m_container.end());

	return count;
}

void SequenceRunner::RefreshSequences()
{
	bool pause = m_paused;
	for (SequenceContainer::reverse_iterator seqIt = m_container.rbegin(); seqIt != m_container.rend(); ++seqIt)
	{
		if (seqIt->paused != pause)
		{
			if (pause)
			{
				seqIt->seq->Pause();
			}
			else
			{
				seqIt->seq->Start();
			}
			seqIt->paused = pause;
		}

		pause = pause || seqIt->seq->IsExclusive();
	}
}
