#pragma once

#include "Sequence.h"

namespace bwn
{

class LoadingMainMenuSeq : public Sequence
{
	//
	// Public constants.
	// 
public:
	static const char*const k_seqName;

	//
	// Construction and destruction.
	//
public:
	LoadingMainMenuSeq(uint32_t priority = Sequence::k_midPrio);

	//
	// Public interface.
	//
public:

	//
	// Private methods.
	//
private:

	//
	// Private Sequence methods.
	//
private:
	// Actual update method of the sequence (returns true if sequence is finished).
	virtual bool Update(real_t delta) override;
	// Called on the sequence start.
	virtual void OnStartImpl() override;
	// Called on sequence pause.
	virtual void OnPauseImpl() override;
	// Implementation of the init method.
	virtual void OnInit() override;
	// Returns unique key connected by the sequence. Used by the unique and parallelUnique seqeuences. 
	virtual Key GetKey() const override;

	//
	// Private members.
	//
private:
};

} // namespace bwn
