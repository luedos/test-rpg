#pragma once

#include "shared_obj_ptr.h"

namespace bwn
{

template<class T> class weak_obj_ptr
{
private:

	// Borland 5.5.1 specific workarounds
	typedef weak_obj_ptr<T> this_type;

public:

	typedef typename detail::sp_element< T >::type element_type;

	constexpr weak_obj_ptr() noexcept 
		: m_obj(nullptr)
		, m_owner(nullptr)
		, m_counter()
	{}

	weak_obj_ptr(weak_obj_ptr const& r) noexcept 
		: m_obj(r.m_obj)
		, m_owner(r.m_owner)
		, m_counter(r.m_counter)
	{
	}

	weak_obj_ptr& operator=(weak_obj_ptr const& r) noexcept
	{
		m_obj = r.m_obj;
		m_owner = r.m_owner;
		m_counter = r.m_counter;
		return *this;
	}

	// for better efficiency in the T == Y case
	weak_obj_ptr(weak_obj_ptr&& r) noexcept 
		: m_obj(r.m_obj)
		, m_owner(r.m_owner)
		, m_counter(std::move(r.m_counter))
	{
		r.m_obj = nullptr;
		r.m_owner = nullptr;
	}

	// for better efficiency in the T == Y case
	weak_obj_ptr& operator=(weak_obj_ptr&& r) noexcept
	{
		this_type(static_cast<weak_obj_ptr&&>(r)).swap(*this);
		return *this;
	}

	weak_obj_ptr(shared_obj_ptr<T> const& r) noexcept
		 : m_obj(r.m_obj)
		 , m_owner(r.m_owner)
		 , m_counter(r.m_counter)
	{}

	weak_obj_ptr& operator=(shared_obj_ptr<T> const& r) noexcept
	{		
		m_obj = r.get_object();
		m_owner = r.get_owner();

		m_counter = m_owner != nullptr ? r.m_counter : detail::weak_count();

		return *this;
	}

	shared_obj_ptr<T> lock() const noexcept
	{
		return shared_obj_ptr<T>(*this);
	}

	long use_count() const noexcept
	{
		return m_counter.use_count();
	}

	bool expired() const noexcept
	{
		return m_counter.use_count() == 0;
	}

	bool empty() const noexcept // extension, not in std::weak_ptr
	{
		return m_counter.empty();
	}

	void reset() noexcept
	{
		this_type().swap(*this);
	}

	void swap(this_type& other) noexcept
	{
		std::swap(m_obj, other.m_obj);
		std::swap(m_owner, other.m_owner);
		m_counter.swap(other.m_counter);
	}

	template<class Y> 
	bool owner_before(weak_obj_ptr<Y> const& rhs) const noexcept
	{
		return m_counter < rhs.m_counter;
	}

	template<class Y> 
	bool owner_before(shared_obj_ptr<Y> const& rhs) const noexcept
	{
		return m_counter < rhs.m_counter;
	}

private:

	template<class Y> friend class weak_obj_ptr;
	template<class Y> friend class shared_obj_ptr;

	element_type* m_obj;
	godot_object* m_owner;
	detail::weak_count m_counter;

};  // weak_ptr

template<class T, class U> 
inline bool operator<(weak_obj_ptr<T> const& a, weak_obj_ptr<U> const& b) noexcept
{
	return a.owner_before(b);
}

template<class T> 
void swap(weak_obj_ptr<T>& a, weak_obj_ptr<T>& b) noexcept
{
	a.swap(b);
}

}