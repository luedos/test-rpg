#pragma once

#include "VariantUtility.h"

#if defined(BWN_DEBUG)
#include <string>
#endif

namespace godot
{

class SceneState;
class PackedScene;
class String;
class Variant;

}

namespace bwn
{
namespace utility
{

class PackedSceneHelper
{
	//
	// Construction and destruction.
	//
public:
	explicit PackedSceneHelper(godot::Ref<godot::PackedScene> scene, int64_t nodeId = 0);

	//
	// Public interface.
	//
public:
	// Changes id of the monitoring node.
	void SetNodeId(int64_t nodeId);
	// Changes id of the monitoring node.
	void ResetScene(godot::Ref<godot::PackedScene> scene);
	// Searches for property of current node, and returns it through out param as Variant (returns true if property was found).
	bool GetProperty(const godot::String& name, godot::Variant& out) const;
	// Searches for property of current node, tries to cast it for respected type, and returns casted type through out value (returns true if property was found, and casted successfully).
	template<typename TypeT>
	auto GetPropertyAs(const godot::String& name, typename VariantTypeHelper<TypeT>::TypeT& out) const 
	-> decltype((void)(std::declval<typename VariantTypeHelper<TypeT>::TypeT>()), std::declval<bool>())
	{
		godot::Variant variant;
		return GetProperty(name, variant) && VariantCastHelper<TypeT>::Cast(variant, out);
	}
	// Returns Type of the native script connected to the node.
	godot::String GetNativeScriptType() const;
	// Returns Base Type of the script connected to the node.
	godot::String GetScriptBaseType() const;
	
	// Returns true if node has connected to it native script of specific type.
	template<typename TypeT>
	bool IsNativeScriptOfType()
	{
		const godot::String typeName = GetNativeScriptType();
		const std::size_t typeTag = godot::_TagDB::get_type_tag_name(typeName);

#if defined(BWN_DEBUG)
		const std::wstring test = typeName.unicode_str();
#endif

		return godot::_TagDB::is_type_compatible(TypeT::___get_id(), typeTag);
	}

	//
	// Private members.
	//
private:
	godot::Ref<godot::PackedScene> m_scene;
	godot::Ref<godot::SceneState> m_state;
	int64_t m_nodeId;
};

} // namespace bwn::utility
} // namespace bwn
