#pragma once

#include "obj_ref_count.h"

namespace bwn
{
template<class T> class weak_obj_ptr;
template<class T> class shared_obj_ptr;

template<class T> class shared_obj_ptr
{
private:

	// Borland 5.5.1 specific workaround
	typedef shared_obj_ptr<T> this_type;

public:

	typedef typename detail::sp_element< T >::type element_type;

	constexpr shared_obj_ptr() noexcept
		: m_obj(nullptr)
		, m_owner(nullptr)
		, m_counter()
	{
	}
	constexpr shared_obj_ptr(std::nullptr_t) noexcept 
		: shared_obj_ptr()
	{
	}

	explicit shared_obj_ptr(element_type* p)
	{
		detail::shared_count(p).swap(m_counter);
		m_obj = p;
		m_owner = m_obj != nullptr ? m_obj->_owner : nullptr;
	}

	shared_obj_ptr(shared_obj_ptr const& r) noexcept 
		: m_obj(r.m_obj)
		, m_owner(r.m_owner)
		, m_counter(r.m_counter)
	{
	}
	
	template< class Y >
	shared_obj_ptr(shared_obj_ptr<Y> const& r, element_type* p) noexcept 
		: shared_obj_ptr()
	{
		if (p == nullptr || p->_owner != r.get_owner()) {
			return;
		}

		m_obj = p;
		m_owner = r.m_owner;
		m_counter = r.m_counter;
	}

	shared_obj_ptr& operator=(shared_obj_ptr const& r) noexcept
	{
		this_type(r).swap(*this);
		return *this;
	}

	// Move support

	shared_obj_ptr(shared_obj_ptr&& r) noexcept 
		: m_obj(r.m_obj)
		, m_owner(r.m_owner)
		, m_counter()
	{
		r.m_owner = nullptr;
		r.m_obj = nullptr;
		m_counter.swap(r.m_counter);
	}

	shared_obj_ptr& operator=(shared_obj_ptr&& r) noexcept
	{
		this_type(static_cast<shared_obj_ptr&&>(r)).swap(*this);
		return *this;
	}

	shared_obj_ptr& operator=(std::nullptr_t) noexcept
	{
		this_type().swap(*this);
		return *this;
	}

	void reset() noexcept
	{
		this_type().swap(*this);
	}

	void reset(element_type* p) // Y must be complete
	{
		assert(p == 0 || p != m_obj); // catch self-reset errors
		this_type(p).swap(*this);
	}

	element_type* get_object() const noexcept
	{
		return is_valid() ? m_obj : nullptr;
	}

	godot_object* get_owner() const noexcept
	{
		return is_valid() ? m_owner : nullptr;
	}

	bool unique() const noexcept
	{
		return m_counter.unique();
	}

	long use_count() const noexcept
	{
		return m_counter.use_count();
	}

	void swap(shared_obj_ptr& other) noexcept
	{
		std::swap(m_obj, other.m_obj);
		std::swap(m_owner, other.m_owner);
		m_counter.swap(other.m_counter);
	}

	bool is_valid() const
	{
		return m_owner != nullptr && godot::core_1_1_api->godot_is_instance_valid(m_owner);
	}

	template<class Y> 
	bool owner_before(shared_obj_ptr<Y> const& rhs) const noexcept
	{
		return m_counter < rhs.m_counter;
	}

private:

	template<class Y> friend class shared_obj_ptr;
	template<class Y> friend class weak_obj_ptr;

	element_type* m_obj;
	godot_object* m_owner;
	detail::shared_count m_counter;    // reference counter

};  // shared_obj_ptr

template<class T, class U> 
inline bool operator==(shared_obj_ptr<T> const& a, shared_obj_ptr<U> const& b) noexcept
{
	return a.get_object() == b.get_object();
}

template<class T, class U> 
inline bool operator!=(shared_obj_ptr<T> const& a, shared_obj_ptr<U> const& b) noexcept
{
	return a.get_object() != b.get_object();
}

template<class T> inline 
bool operator==(shared_obj_ptr<T> const& p, std::nullptr_t) noexcept
{
	return p.get_object() == 0;
}

template<class T>
inline bool operator==(std::nullptr_t, shared_obj_ptr<T> const& p) noexcept
{
	return p.get_object() == 0;
}

template<class T> 
inline bool operator!=(shared_obj_ptr<T> const& p, std::nullptr_t) noexcept
{
	return p.get_object() != 0;
}

template<class T> 
inline bool operator!=(std::nullptr_t, shared_obj_ptr<T> const& p) noexcept
{
	return p.get_object() != 0;
}

template<class T, class U> 
inline bool operator<(shared_obj_ptr<T> const& a, shared_obj_ptr<U> const& b) noexcept
{
	return a.owner_before(b);
}

template<class T> 
inline void swap(shared_obj_ptr<T>& a, shared_obj_ptr<T>& b) noexcept
{
	a.swap(b);
}

template<class T, class U>
shared_obj_ptr<T> object_pointer_cast(shared_obj_ptr<U> const& r) noexcept
{
	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = godot::Object::cast_to<E>(r.get_object());

	return shared_obj_ptr<T>(r, p);
}

template<class T, class U> 
shared_obj_ptr<T> static_pointer_cast(shared_obj_ptr<U> const& r) noexcept
{
	(void) static_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = static_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(r, p);
}

template<class T, class U> 
shared_obj_ptr<T> const_pointer_cast(shared_obj_ptr<U> const& r) noexcept
{
	(void) const_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = const_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(r, p);
}

template<class T, class U> 
shared_obj_ptr<T> dynamic_pointer_cast(shared_obj_ptr<U> const& r) noexcept
{
	(void) dynamic_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = dynamic_cast<E*>(r.get_object());
	return p ? shared_obj_ptr<T>(r, p) : shared_obj_ptr<T>();
}

template<class T, class U> 
shared_obj_ptr<T> reinterpret_pointer_cast(shared_obj_ptr<U> const& r) noexcept
{
	(void) reinterpret_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = reinterpret_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(r, p);
}

template<class T, class U> 
shared_obj_ptr<T> static_pointer_cast(shared_obj_ptr<U>&& r) noexcept
{
	(void) static_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = static_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(std::move(r), p);
}

template<class T, class U> 
shared_obj_ptr<T> const_pointer_cast(shared_obj_ptr<U>&& r) noexcept
{
	(void) const_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = const_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(std::move(r), p);
}

template<class T, class U> 
shared_obj_ptr<T> dynamic_pointer_cast(shared_obj_ptr<U>&& r) noexcept
{
	(void) dynamic_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = dynamic_cast<E*>(r.get_object());
	return p ? shared_obj_ptr<T>(std::move(r), p) : shared_obj_ptr<T>();
}

template<class T, class U> 
shared_obj_ptr<T> reinterpret_pointer_cast(shared_obj_ptr<U>&& r) noexcept
{
	(void) reinterpret_cast<T*>(static_cast<U*>(0));

	typedef typename shared_obj_ptr<T>::element_type E;

	E* p = reinterpret_cast<E*>(r.get_object());
	return shared_obj_ptr<T>(std::move(r), p);
}

} // namespace bwn
