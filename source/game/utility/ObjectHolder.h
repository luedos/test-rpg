#pragma once

namespace godot
{

class Object;

}

namespace bwn
{

namespace detail
{
	bool CheckObject(const void* object);
	void DestroyObject(void* object);
}

template<typename TypeT>
class ObjectHolder
{
	//
	// Construction and destruction.
	//
public:
	// Simple constructor.
	ObjectHolder()
		: m_obj{ nullptr }
		, m_owner{ nullptr }
	{}
	// This one is basically used only for compatability with godot parameters.
	ObjectHolder(const godot::Variant& variant)
	{
		ResetObj(variant);
	}
	// Same as reset, but simpler.
	ObjectHolder(TypeT* object)
	{
		ResetObj(object);
	}
	// Same as reset, but simpler.
	ObjectHolder& operator=(TypeT* object)
	{
		ResetObj(object);
		return *this;
	}
	// This one is basically used only for compatability with godot parameters.
	ObjectHolder& operator=(const godot::Variant& variant)
	{
		ResetObj(variant);
		return *this;
	}
	// Does not transfer data if other is invalid, which is mean: if other is dirty but invalid, copy will be not dirty and invalid. 
	ObjectHolder(const ObjectHolder& other)
	{
		const bool valid = other.IsObjectValid();
		m_owner = valid ? other.m_owner : nullptr;
		m_obj = valid ? other.m_obj : nullptr;
	}
	// Does not transfer data if other is invalid, which is mean: if other is dirty but invalid, the copy will be not dirty. 
	ObjectHolder& operator=(const ObjectHolder& other)
	{
		if (&other != this) 
		{
			const bool valid = other.IsObjectValid();
			m_owner = valid ? other.m_owner : nullptr;
			m_obj = valid ? other.m_obj : nullptr;
		}

		return *this;
	}

	// Full transfer of data, if one was dirty, the other will be as well.
	ObjectHolder(ObjectHolder&& other)
		: m_obj{ other.m_obj }
		, m_owner{ other.m_owner }
	{
		other.m_obj = nullptr;
		other.m_owner = nullptr;
	}
	// Full transfer of data, if one was dirty, the other will be as well.
	ObjectHolder& operator=(ObjectHolder&& other)
	{
		if (&other != this) 
		{
			m_owner = other.m_owner;
			m_obj = other.m_obj;

			other.m_obj = nullptr;
			other.m_owner = nullptr;
		}

		return *this;
	}

	//
	// Public Interface
	//
public:
	// Returns true if holder has an object (doesn't account if object is valid or not).
	bool IsDirty() const
	{
		return m_owner != nullptr;
	}
	// Returns true if object is stored and is valid.
	bool IsObjectValid() const
	{
		return IsDirty() && detail::CheckObject(m_owner);
	}
	// Returns object if it's valid, otherwise returns nullptr.
	const TypeT* GetObject() const
	{
		return IsObjectValid() ? m_obj : nullptr;
	}
	// Returns object if it's valid, otherwise returns nullptr.
	TypeT* GetObject()
	{
		return IsObjectValid() ? m_obj : nullptr;
	}
	// Returns owner of object if it's valid, otherwise returns nullptr.
	const void* GetOwner() const
	{
		return IsObjectValid() ? m_owner : nullptr;
	}
	// Returns owner of object if it's valid, otherwise returns nullptr.
	void* GetOwner()
	{
		return IsObjectValid() ? m_owner : nullptr;
	}
	// Resets object with another (if object is invalid, it's equal as reset with nullptr).
	void ResetObj(TypeT* object)
	{
		const bool objectExist = object != nullptr && detail::CheckObject(object->_owner);

		m_obj = objectExist ? object : nullptr;
		m_owner = objectExist ? object->_owner : nullptr;		
	}
	// This one is basically used only for compatability with godot parameters.
	operator godot::Variant() const
	{
		return godot::Variant{ GetObject() };
	}

	//
	// Private members.
	//
private:
	// The actual object.
	TypeT* m_obj;
	// The owner. It's saved separately because we can't be sure then actual object will be deleted, but, simply by checking owner we can find it out. 
	// How does godot saves all owners in separate pointers, and there is never a memory collision is a mystery.. 
	// mb owner is not actually a pointer, but a index of type std::size_t, and only stored as void* (same as type_tag).
	void* m_owner;
};


}
