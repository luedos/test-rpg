#pragma once

#include "ManualSingleton.h"

namespace bwn
{

template<typename TypeT>
class AutoSingleton : private ManualSingleton<TypeT>
{
	//
	// Private aliasing.
	//
public:
	using ManualSingleton<TypeT>::GetInstance;

	//
	// Construction and destruction.
	//
public:
	// Simple constructor.
	AutoSingleton()
	{
		TakeInstance();
	}
	// Simple destructor.
	~AutoSingleton()
	{
		ResetInstance();
	}
};

} // namespace bwn