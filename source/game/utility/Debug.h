#pragma once

namespace bwn
{

bool Assert(const char* expr, const char* msg, const char* function, const char* file, uint32_t line);
void PrintLog(const char* msg, const char* function, const char* file, uint32_t line);
void PrintWarning(const char* cond, const char* msg, const char* function, const char* file, uint32_t line);
void PrintError(const char* cond, const char* msg, const char* function, const char* file, uint32_t line);

} // namespace bwn