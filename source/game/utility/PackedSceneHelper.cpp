#include "pch.h"

#include "PackedSceneHelper.h"

using namespace bwn;

utility::PackedSceneHelper::PackedSceneHelper(godot::Ref<godot::PackedScene> scene, int64_t nodeId)
{
	ResetScene(scene);
	SetNodeId(nodeId);
}

void utility::PackedSceneHelper::ResetScene(godot::Ref<godot::PackedScene> scene)
{
	m_scene = scene;
	m_state = m_scene.is_valid() ? m_scene->get_state() : nullptr;
}

void utility::PackedSceneHelper::SetNodeId(int64_t nodeId)
{
	m_nodeId = nodeId;
}

bool utility::PackedSceneHelper::GetProperty(const godot::String& name, godot::Variant& out) const
{
	if (!m_state.is_valid()) {
		return false;
	}

	const int64_t count = m_state->get_node_property_count(m_nodeId);

	for (int64_t propId = 0; propId < count; ++propId)
	{
		if (m_state->get_node_property_name(m_nodeId, propId) == name)
		{
			out = m_state->get_node_property_value(m_nodeId, propId);
			return true;
		}
	}

	return false;
}

godot::String utility::PackedSceneHelper::GetNativeScriptType() const
{
	godot::String ret;
	const godot::Script* script;
	if(GetPropertyAs<const godot::Script>("script", script))
	{
		const godot::NativeScript*const native = godot::Object::cast_to<godot::NativeScript>(script);
		if (native != nullptr)
		{
			ret = native->get_class_name();
		}
	}

#if defined(BWN_DEBUG)
	const std::wstring testName = ret.unicode_str();
#endif

	return ret;
}

godot::String utility::PackedSceneHelper::GetScriptBaseType() const
{
	godot::String ret;
	const godot::Script* script;
	if(GetPropertyAs<const godot::Script>("script", script))
	{
		ret = script->get_instance_base_type();
	}

	return ret;
}
