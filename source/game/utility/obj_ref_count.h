#pragma once

namespace bwn
{
// Mostly just boost implementation of shared counter.
namespace detail
{

inline void atomic_increment(std::atomic_int_least32_t* pw) noexcept
{
	pw->fetch_add(1, std::memory_order_relaxed);
}

inline std::int_least32_t atomic_decrement(std::atomic_int_least32_t* pw) noexcept
{
	return pw->fetch_sub(1, std::memory_order_acq_rel);
}

inline std::int_least32_t atomic_conditional_increment(std::atomic_int_least32_t* pw) noexcept
{
	std::int_least32_t r = pw->load(std::memory_order_relaxed);

	for (;; )
	{
		if (r == 0)
		{
			return r;
		}

		if (pw->compare_exchange_weak(r, r + 1, std::memory_order_relaxed, std::memory_order_relaxed))
		{
			return r;
		}
	}
}

class sp_counted_base
{
private:

	sp_counted_base(sp_counted_base const&);
	sp_counted_base& operator= (sp_counted_base const&);

	std::atomic_int_least32_t use_count_;
	std::atomic_int_least32_t weak_count_;

	godot_object* owner_;

public:

	sp_counted_base(godot_object* owner) noexcept 
		: use_count_(1)
		, weak_count_(1)
		, owner_( owner )
	{
	}

	~sp_counted_base() /*BOOST_SP_NOEXCEPT*/
	{
	}

	void dispose() noexcept
	{
		if (is_valid())
		{
			godot::api->godot_object_destroy(owner_);
		}

		owner_ = nullptr;
	}

	void destroy() noexcept
	{
		delete this;
	}

	void add_ref_copy() noexcept
	{
		atomic_increment(&use_count_);
	}

	bool add_ref_lock() noexcept // true on success
	{
		return atomic_conditional_increment(&use_count_) != 0;
	}

	void release() noexcept
	{
		if (atomic_decrement(&use_count_) == 1)
		{
			dispose();
			weak_release();
		}
	}

	void weak_add_ref() noexcept
	{
		atomic_increment(&weak_count_);
	}

	void weak_release() noexcept
	{
		if (atomic_decrement(&weak_count_) == 1)
		{
			destroy();
		}
	}

	long use_count() const noexcept
	{
		return use_count_.load(std::memory_order_acquire);
	}

	bool is_valid() const noexcept
	{
		return owner_ != nullptr && godot::core_1_1_api->godot_is_instance_valid(owner_);
	}

	void* get_owner() noexcept
	{
		return owner_;
	}

	const void* get_owner() const noexcept
	{
		return owner_;
	}
};

//=======================================================
//=======================// counter //===================
//=======================================================


class weak_count;

class shared_count
{
private:

	sp_counted_base* counter_;

	friend class weak_count;

public:

	constexpr shared_count() noexcept : counter_(0)
	{
	}

	constexpr explicit shared_count(sp_counted_base* counter) noexcept: counter_(counter)
	{
	}

	template<class TypeT> explicit shared_count(TypeT* obj) : counter_(0)
	{
		if (obj == nullptr)
		{
			return;
		}

		counter_ = new sp_counted_base(obj->_owner);
	}

	~shared_count() /*BOOST_SP_NOEXCEPT*/
	{
		if (counter_ != nullptr) counter_->release();
	}

	shared_count(shared_count const& r) noexcept: counter_(r.counter_)
	{
		if (counter_ != 0) counter_->add_ref_copy();
	}

	shared_count(shared_count&& r) noexcept : counter_(r.counter_)
	{
		r.counter_ = 0;
	}

	inline explicit shared_count(weak_count const& r) noexcept;

	shared_count& operator= (shared_count const& r) noexcept
	{
		sp_counted_base* tmp = r.counter_;

		if (tmp != counter_)
		{
			if (tmp != 0) tmp->add_ref_copy();
			if (counter_ != 0) counter_->release();
			counter_ = tmp;
		}

		return *this;
	}

	void swap(shared_count& r) noexcept
	{
		sp_counted_base* tmp = r.counter_;
		r.counter_ = counter_;
		counter_ = tmp;
	}

	long use_count() const noexcept
	{
		return counter_ != 0 ? counter_->use_count() : 0;
	}

	bool unique() const noexcept
	{
		return use_count() == 1;
	}

	friend inline bool operator==(shared_count const& a, shared_count const& b) noexcept
	{
		return a.counter_ == b.counter_;
	}

	friend inline bool operator<(shared_count const& a, shared_count const& b) noexcept
	{
		return std::less<sp_counted_base*>()(a.counter_, b.counter_);
	}

	bool is_valid() const noexcept
	{
		return counter_ != nullptr && counter_->is_valid(); 
	}

	void* get_owner() noexcept
	{
		return counter_ != nullptr ? counter_->get_owner() : nullptr;
	}

	const void* get_owner() const noexcept 
	{
		return counter_ != nullptr ? counter_->get_owner() : nullptr;
	}
};


class weak_count
{
private:

	sp_counted_base* counter_;

	friend class shared_count;

public:

	constexpr weak_count() noexcept : counter_(0)
	{
	}

	weak_count(shared_count const& r) noexcept: counter_(r.counter_)
	{
		if (counter_ != 0) counter_->weak_add_ref();
	}

	weak_count(weak_count const& r) noexcept: counter_(r.counter_)
	{
		if (counter_ != 0) counter_->weak_add_ref();
	}

	// Move support
	weak_count(weak_count&& r) noexcept: counter_(r.counter_)
	{
		r.counter_ = 0;
	}

	~weak_count() /*BOOST_SP_NOEXCEPT*/
	{
		if (counter_ != 0) counter_->weak_release();
	}

	weak_count& operator= (shared_count const& r) noexcept
	{
		sp_counted_base* tmp = r.counter_;

		if (tmp != counter_)
		{
			if (tmp != 0) tmp->weak_add_ref();
			if (counter_ != 0) counter_->weak_release();
			counter_ = tmp;
		}

		return *this;
	}

	weak_count& operator= (weak_count const& r) noexcept
	{
		sp_counted_base* tmp = r.counter_;

		if (tmp != counter_)
		{
			if (tmp != 0) tmp->weak_add_ref();
			if (counter_ != 0) counter_->weak_release();
			counter_ = tmp;
		}

		return *this;
	}

	void swap(weak_count& r) noexcept
	{
		sp_counted_base* tmp = r.counter_;
		r.counter_ = counter_;
		counter_ = tmp;
	}

	long use_count() const noexcept
	{
		return counter_ != 0 ? counter_->use_count() : 0;
	}

	bool empty() const noexcept
	{
		return counter_ == 0;
	}

	friend inline bool operator==(weak_count const& a, weak_count const& b) noexcept
	{
		return a.counter_ == b.counter_;
	}

	friend inline bool operator<(weak_count const& a, weak_count const& b) noexcept
	{
		return std::less<sp_counted_base*>()(a.counter_, b.counter_);
	}
};

inline shared_count::shared_count(weak_count const& r) noexcept : counter_(r.counter_)
{
	if (counter_ != 0 && !counter_->add_ref_lock())
	{
		counter_ = 0;
	}
}

template< class T > struct sp_element
{
	typedef T type;
};

template< class Y, class T > struct sp_convertible
{
	typedef char(&yes)[1];
	typedef char(&no)[2];

	static yes f(T*);
	static no  f(...);

	enum _vt { value = sizeof((f)(static_cast<Y*>(0))) == sizeof(yes) };
};

struct sp_empty
{
};

template< class Y, class T > inline void sp_assert_convertible() noexcept
{
	static_assert( sp_convertible< Y, T >::value );
	typedef char tmp[sp_convertible< Y, T >::value ? 1 : -1];
	(void)sizeof(tmp);
}

} // namespace detail

} // namespace bwn