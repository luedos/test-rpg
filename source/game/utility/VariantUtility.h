#pragma once

#include <Variant.hpp>

namespace bwn
{
namespace utility
{

template<typename, typename = std::void_t<>>
struct VariantTypeHelper;

template<> struct VariantTypeHelper<bool, std::void_t<>> 				{ enum { value = godot::Variant::BOOL }; using TypeT = bool; };

template<> struct VariantTypeHelper<int8_t, std::void_t<>> 				{ enum { value = godot::Variant::INT }; using TypeT = int8_t; };
template<> struct VariantTypeHelper<int16_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = int16_t; };
template<> struct VariantTypeHelper<int32_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = int32_t; };
template<> struct VariantTypeHelper<int64_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = int64_t; };

template<> struct VariantTypeHelper<uint8_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = uint8_t; };
template<> struct VariantTypeHelper<uint16_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = uint16_t; };
template<> struct VariantTypeHelper<uint32_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = uint32_t; };
template<> struct VariantTypeHelper<uint64_t, std::void_t<>> 			{ enum { value = godot::Variant::INT }; using TypeT = uint64_t; };

template<> struct VariantTypeHelper<real_t, std::void_t<>> 				{ enum { value = godot::Variant::REAL }; using TypeT = real_t; };
template<> struct VariantTypeHelper<double, std::void_t<>> 				{ enum { value = godot::Variant::REAL }; using TypeT = double; };

template<> struct VariantTypeHelper<godot::String, std::void_t<>> 		{ enum { value = godot::Variant::STRING }; using TypeT = godot::String; };

template<> struct VariantTypeHelper<godot::Vector2, std::void_t<>> 		{ enum { value = godot::Variant::VECTOR2 }; using TypeT = godot::Vector2; };
template<> struct VariantTypeHelper<godot::Vector3, std::void_t<>> 		{ enum { value = godot::Variant::VECTOR3 }; using TypeT = godot::Vector3; };

template<> struct VariantTypeHelper<godot::Rect2, std::void_t<>> 		{ enum { value = godot::Variant::RECT2 }; using TypeT = godot::Rect2; };
//template<> struct VariantTypeHelper<godot::Rect3, std::void_t<>> 		{ enum { value = godot::Variant::RECT3 }; using TypeT = godot::Rect3; };

template<> struct VariantTypeHelper<godot::Plane, std::void_t<>> 		{ enum { value = godot::Variant::PLANE }; using TypeT = godot::Plane;};
template<> struct VariantTypeHelper<godot::Quat, std::void_t<>> 		{ enum { value = godot::Variant::QUAT }; using TypeT = godot::Quat; };
template<> struct VariantTypeHelper<godot::Basis, std::void_t<>> 		{ enum { value = godot::Variant::BASIS }; using TypeT = godot::Basis; };

template<> struct VariantTypeHelper<godot::Transform2D, std::void_t<>> 	{ enum { value = godot::Variant::TRANSFORM2D }; using TypeT = godot::Transform2D; };
template<> struct VariantTypeHelper<godot::Transform, std::void_t<>> 	{ enum { value = godot::Variant::TRANSFORM }; using TypeT = godot::Transform; };

template<> struct VariantTypeHelper<godot::Color, std::void_t<>> 		{ enum { value = godot::Variant::COLOR }; using TypeT = godot::Color; };

template<> struct VariantTypeHelper<godot::NodePath, std::void_t<>> 	{ enum { value = godot::Variant::NODE_PATH }; using TypeT = godot::NodePath; };

template<> struct VariantTypeHelper<godot::RID, std::void_t<>> 			{ enum { value = godot::Variant::_RID }; using TypeT = godot::RID; };

template<> struct VariantTypeHelper<godot::Dictionary, std::void_t<>> 	{ enum { value = godot::Variant::DICTIONARY }; using TypeT = godot::Dictionary; };
template<> struct VariantTypeHelper<godot::Array, std::void_t<>> 		{ enum { value = godot::Variant::ARRAY }; using TypeT = godot::Array; };

template<> struct VariantTypeHelper<godot::PoolByteArray, std::void_t<>> 	{ enum { value = godot::Variant::POOL_BYTE_ARRAY }; using TypeT = godot::PoolByteArray; };
template<> struct VariantTypeHelper<godot::PoolIntArray, std::void_t<>> 	{ enum { value = godot::Variant::POOL_INT_ARRAY }; using TypeT = godot::PoolIntArray; };
template<> struct VariantTypeHelper<godot::PoolRealArray, std::void_t<>> 	{ enum { value = godot::Variant::POOL_REAL_ARRAY }; using TypeT = godot::PoolRealArray; };
template<> struct VariantTypeHelper<godot::PoolStringArray, std::void_t<>> 	{ enum { value = godot::Variant::POOL_STRING_ARRAY }; using TypeT = godot::PoolStringArray; };
template<> struct VariantTypeHelper<godot::PoolVector2Array, std::void_t<>> { enum { value = godot::Variant::POOL_VECTOR2_ARRAY }; using TypeT = godot::PoolVector2Array; };
template<> struct VariantTypeHelper<godot::PoolVector3Array, std::void_t<>> { enum { value = godot::Variant::POOL_VECTOR3_ARRAY }; using TypeT = godot::PoolVector3Array; };
template<> struct VariantTypeHelper<godot::PoolColorArray, std::void_t<>> 	{ enum { value = godot::Variant::POOL_COLOR_ARRAY }; using TypeT = godot::PoolColorArray; };

template<typename ObjectT>
struct VariantTypeHelper<ObjectT, typename std::enable_if<std::is_base_of<godot::Object, ObjectT>::value>::type>
{ enum { value = godot::Variant::OBJECT }; using TypeT = typename ObjectT*; };

// This class should only work for non godot::Object classes (like uint/String etc.).
template<typename ObjectT>
struct VariantTypeHelper<const ObjectT, typename std::void_t<typename std::enable_if<VariantTypeHelper<ObjectT>::value != godot::Variant::OBJECT>::type>>
{ enum { value =  VariantTypeHelper<ObjectT>::value }; using TypeT = typename VariantTypeHelper<ObjectT>::TypeT; };

template<typename TypeT, typename = std::void_t<>>
struct VariantCastHelper
{
	static bool Cast(const godot::Variant& variant, TypeT& out)
	{
		const bool canCast = variant.get_type() == VariantTypeHelper<TypeT>::value; 
		if (canCast)
		{
			out = variant;
		}
		return canCast;
	}
};

template<typename ObjectT>
struct VariantCastHelper<ObjectT, typename std::enable_if<std::is_base_of<godot::Object, ObjectT>::value>::type>
{
	static bool Cast(const godot::Variant& variant, ObjectT*& out)
	{
		if (variant.get_type() == godot::Variant::OBJECT)
		{
			godot::Object*const obj = variant;
			ObjectT*const casted = godot::Object::cast_to<ObjectT>(obj);
		
			if (casted != nullptr)
			{
				out = casted;
				return true;
			}
		}

		return false;
	}
};

} // namepsace bwn::utility
} // namespace bwn