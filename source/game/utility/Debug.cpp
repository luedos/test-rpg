#include "pch.h"

#if defined(BWN_WINDOWS_SYSTEM)

#include <Windows.h>
#include <WinUser.h>

#endif // BWN_WINDOWS_SYSTEM

#include "Debug.h"

const char* ExtractFileName(const char* path)
{
	if (path == nullptr) {
		return nullptr;
	}

	const char* ret = path;
	do
	{
		if (*path == '\\' || *path == '/') {
			ret = path + 1;
		}
	}
	while(*path++);

	return ret;
}

#if defined(BWN_WINDOWS_SYSTEM)

bool bwn::Assert(const char* expr, const char* msg, const char* function, const char* file, uint32_t line)
{
	const std::string caption = [file, line]() -> std::string
	{
		std::stringstream stream;
		stream << "[BWN] Assert!: "<< ExtractFileName(file) << ':' << line;
		return stream.str();
	}();
	const std::string message = [expr, function, msg]() -> std::string
	{
		std::stringstream stream;
		stream << "In: " << function << "\nExpression: " << expr << "\nMessage: " << msg;
		return stream.str();
	}();

	const int result = MessageBox(0, message.c_str(), caption.c_str(), MB_ICONEXCLAMATION | MB_ABORTRETRYIGNORE | MB_TASKMODAL | MB_SETFOREGROUND);

	if (result == IDABORT)
	{
		return true;
	}
	else if (result == IDRETRY)
	{
		return true;
	}

	// else if (result == IDIGNORE)
	return false;
}

#else // BWN_WINDOWS_SYSTEM

bool bwn::Assert(const char* expr, const char* msg, const char* function, const char* file, uint32_t line)
{
	return true;
}

#endif // BWN_WINDOWS_SYSTEM

void bwn::PrintLog(const char* msg, const char* function, const char* file, uint32_t line)
{
	const std::string message = [msg, function, file, line]() -> std::string
	{
		std::stringstream stream;
		stream << "[BWN][LOG][" << ExtractFileName(file) << ':' << line << "][" << function << "] " << msg;
		return stream.str();
	}();

	godot::Godot::print(message.c_str());
}

void bwn::PrintWarning(const char* cond,const char* msg, const char* function, const char* file, uint32_t line)
{
	const std::string message = [cond, msg, function, file, line]() -> std::string
	{
		std::stringstream stream;
		// Godot format warning messages for us.
		//stream << "[BWN][WARNING][" << ExtractFileName(file) << ':' << line << "][" << function << "] " << msg;
		stream << "[BWN]";
		if(cond != nullptr) {
			stream << '[' << cond << ']'; 
		} 
		stream << ' ' << msg;
		return stream.str();
	}();

	godot::Godot::print_warning(message.c_str(), function, file, line);
}

void bwn::PrintError(const char* cond,const char* msg, const char* function, const char* file, uint32_t line)
{
	const std::string message = [cond, msg, function, file, line]() -> std::string
	{
		std::stringstream stream;
		// Godot format error messages for us.
		//stream << "[BWN][ERROR][" << ExtractFileName(file) << ':' << line << "][" << function << "] " << msg;
		stream << "[BWN]";
		if(cond != nullptr) {
			stream << '[' << cond << ']'; 
		} 
		stream << ' ' << msg;
		return stream.str();
	}();

	godot::Godot::print_error(message.c_str(), function, file, line);
}
