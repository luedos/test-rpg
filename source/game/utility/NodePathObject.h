#pragma once

#include "ObjectHolder.h"

namespace bwn
{

template<typename NodeT>
class NodePathObject : private ObjectHolder<NodeT>
{
	//
	// Object holder methods.
	//
public:
	using ObjectHolder<NodeT>::IsObjectValid;
	using ObjectHolder<NodeT>::GetObject;
	using ObjectHolder<NodeT>::GetOwner;
	using ObjectHolder<NodeT>::ResetObj;

	//
	// Construction and destruction.
	//
public:
	// Simple constructor.
	NodePathObject()
		: ObjectHolder{ nullptr }
		, m_path{}
		, m_owner{ nullptr }
	{}
	// Constructor for Variant convertion.
	NodePathObject(const godot::Variant& variant)
		: ObjectHolder{ nullptr }
		, m_owner{ nullptr }
	{
		SetPath(variant);
	}

	//
	// Public interface.
	//
public:
	// Resets node from cashed path, returns true if node was reset successfully.
	bool ResetFromPath(godot::Node& parent)
	{
		NodeT*const obj = godot::Object::cast_to<NodeT>(parent.get_node_or_null(m_path));
		BWN_LOG_WARNING_COND(obj != nullptr || m_path.is_empty(), "Can't find node \"" << m_path.operator godot::String().ascii().get_data() << "\".");
		
		ResetObj(obj);
		return IsObjectValid();
	}
	// Sets owner for automatic reset of the paremeter, whenever path was changed. If owner is nullptr, then the only way to reset object is through ResetFromPath.
	// Lifetime of the owner is not tracked!
	void SetOwner(godot::Node* owner)
	{
		m_owner = owner;
	}
	// Returns cashed path.
	godot::NodePath GetPath() const
	{
		return m_path;
	}
	// Sets cashed path, and resets object.
	void SetPath(const godot::NodePath& path)
	{
		m_path = path;

#if defined(BWN_DEBUG)
	const std::wstring test = godot::String(m_path).unicode_str();
#endif

		if (m_owner != nullptr)
		{
			NodeT*const obj = godot::Object::cast_to<NodeT>(m_owner->get_node_or_null(m_path));
			BWN_LOG_WARNING_COND(obj != nullptr || m_path.is_empty(), "Can't find node \"" << m_path.operator godot::String().ascii().get_data() << "\".");
			ResetObj(obj);
		}
	}
	// Conversion of the path to the variant.
	operator godot::Variant() const
	{
		return godot::Variant{ m_path };
	}
	
	//
	// Private members.
	//
private:
	// Path of the object.
	godot::NodePath m_path;
	// The owner from which the path will be searched.
	godot::Node* m_owner;
};

} // namespace bwn.