#include "pch.h"

#include "ObjectHolder.h"
#include <Godot.hpp>

#include "obj_ref_count.h"
#include "shared_obj_ptr.h"
#include "weak_obj_ptr.h"

using namespace bwn;

bool detail::CheckObject(const void* object)
{
	return godot::core_1_1_api->godot_is_instance_valid(object);
}

void detail::DestroyObject(void* object)
{
	godot::api->godot_object_destroy(object);
}
