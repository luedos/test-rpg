#pragma once

namespace bwn
{

template<typename TypeT>
class ManualSingleton
{
	//
	// Construction and destruction.
	//
public:
	// Simple constructor.
	ManualSingleton() = default;
	// Simple destructor.
	~ManualSingleton()
	{
		if (s_instance == static_cast<TypeT*>(this))
		{
			BWN_LOG_WARNING("Instance of singleton \"" << TypeT::___get_type_name() << "\" is deleted without reset.");
			ResetInstance();
		}
	}

	//
	// Public interface.
	// 
	static TypeT* GetInstance()
	{
		BWN_ASSERT_WITH_MSG(s_instance != nullptr, "Instance of the singleton \"" << TypeT::___get_type_name() << "\" is absent.");
		return s_instance;
	} 

	//
	// Protected interface.
	//
protected:
	// Sets instance as self. Returns true if instance was successfully taken.
	bool TakeInstance()
	{
		BWN_ASSERT_WITH_MSG(s_instance == nullptr, "Another instance of the singleton \"" << TypeT::___get_type_name() << "\" is already exist.");
		if (s_instance == nullptr)
		{
			BWN_LOG("Singleton instance of \"" << TypeT::___get_type_name() << "\" is seted.");
			s_instance = static_cast<TypeT*>(this);
		}

		return s_instance == static_cast<TypeT*>(this);
	}
	// Resets instance, if the instance is this. Returns true if instance was successfully reseted.
	bool ResetInstance()
	{
		BWN_ASSERT_WITH_MSG(s_instance == static_cast<TypeT*>(this), "Duplicate instance of the singleton \"" << TypeT::___get_type_name() << "\" can't reset instance.");
		if (s_instance == static_cast<TypeT*>(this))
		{
			BWN_LOG("Singleton instance of \"" << TypeT::___get_type_name() << "\" reseted.");
			s_instance = nullptr;
		}

		return s_instance == nullptr;
	}

	//
	// Private members.
	// 
private:
	static TypeT* s_instance;
};

} // namespace bwn

template<typename TypeT>
TypeT* bwn::ManualSingleton<TypeT>::s_instance = nullptr;