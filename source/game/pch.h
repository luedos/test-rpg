
//
// STD includes.
//
// general.
#include <stdint.h>
#include <type_traits>
#include <assert.h>
#include <algorithm>
// memory handling.
#include <memory>
#include <vector>
#include <map>
#include <unordered_map>
#include <sstream>
// multithreading.
#include <atomic>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>

//
// Godot includes.
//
#include <Godot.hpp>
// general. 
#include <ClassDB.hpp>
#include <Input.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
// nodes.
#include <AnimationPlayer.hpp>
#include <KinematicBody2D.hpp>
#include <StaticBody2D.hpp>
#include <Viewport.hpp>
// resources.
#include <NativeScript.hpp>
#include <PackedScene.hpp>
#include <ResourceInteractiveLoader.hpp>
#include <SceneState.hpp>


//
// BWN definitions.
//
#include "utility/Debug.h"

#if defined(BWN_DEBUG)

#if defined(BWN_WINDOWS_SYSTEM)
#define BWN_BREAK() __debugbreak()
#else // BWN_WINDOWS_SYSTEM
#define BWN_BREAK() std::abort()
#endif // BWN_WINDOWS_SYSTEM

#else // BWN_DEBUG

#define BWN_BREAK() std::abort()

#endif // BWN_DEBUG

#if defined(BWN_DEBUG)

#define BWN_LOG(message) \
do { \
	std::stringstream stream; stream << message; std::string str = stream.str(); \
	bwn::PrintLog(str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
} while(false)

#define BWN_LOG_WARNING(message) \
do { \
	std::stringstream stream; stream << message; std::string str = stream.str(); \
	bwn::PrintWarning(nullptr, str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
} while(false)

#define BWN_LOG_WARNING_COND(cond, message) \
do { \
	if (!(cond)) \
	{\
		std::stringstream stream; stream << message; std::string str = stream.str(); \
		bwn::PrintWarning(#cond, str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
	} \
} while(false)

#define BWN_LOG_ERROR(message) \
do { \
	std::stringstream stream; stream << message; std::string str = stream.str(); \
	bwn::PrintError(nullptr, str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
} while(false)

#define BWN_LOG_ERROR_COND(cond, message) \
do { \
	if (!(cond)) \
	{\
		std::stringstream stream; stream << message; std::string str = stream.str(); \
		bwn::PrintError(#cond, str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
	} \
} while(false)

#define BWN_ASSERT_WITH_MSG(cond, message) \
do { if (!(cond)) { \
	std::stringstream stream; stream << message; std::string str = stream.str(); \
	bwn::PrintError(#cond, str.c_str(), __FUNCTION__, __FILE__, __LINE__); \
	if (bwn::Assert(#cond, str.c_str(), __FUNCTION__, __FILE__, __LINE__)) BWN_BREAK();; \
} } while(false)

#else // BWN_DEBUG

#define BWN_LOG(message) (void)(0)
#define BWN_LOG_WARNING(message) (void)(0)
#define BWN_LOG_ERROR(message) (void)(0)
#define BWN_ASSERT_WITH_MSG(message) (void)(0)

#endif // BWN_DEBUG

//
// Utility includes.
//
// object holders.
#include "utility/shared_obj_ptr.h"
#include "utility/weak_obj_ptr.h"
#include "utility/ObjectHolder.h"
#include "utility/NodePathObject.h"