#pragma once

#include "utility/AutoSingleton.h"

namespace bwn
{

class SaveMgr : public godot::Node, public AutoSingleton<SaveMgr>
{
	GODOT_CLASS(SaveMgr, godot::Node)

	//
	// Public constraction and destruction.
	//
public:
	// Constructor.
	SaveMgr();
	// Destructor.
	~SaveMgr();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init();
	void _ready();
	void _process(real_t delta);

	//
	// Public interface.
	//
public:
	godot::String GetCurrentRoomId() const;	

	//
	// Private methods.
	//
private:

	//
	// Private members.
	//
private:
	// TODO: Temporary current room id.
	godot::String temp_roomId;
};

} // namespace bwn