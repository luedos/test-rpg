#pragma once

#include "utility/AutoSingleton.h"

namespace bwn 
{
class RoomAsyncLoader;
class RoomHandler;
class Room;

class RoomMgr : public godot::Node, public bwn::AutoSingleton<RoomMgr>
{
	GODOT_CLASS(RoomMgr, godot::Node)

	//
	// Public aliases.
	//
public:
	using RoomHandlerPtr = std::shared_ptr<RoomHandler>;
	using RoomHandlerWeakPtr = std::weak_ptr<RoomHandler>;
	
	//
	// Private aliases.
	//
private:
	using LoaderPtr = std::shared_ptr<RoomAsyncLoader>;
	using WeightedAsyncLoader = std::pair<uint32_t, LoaderPtr>;
	using WeightetLoadersStack = std::vector<WeightedAsyncLoader>;

	using RoomHandlersMap = std::map<godot::String, RoomHandlerPtr>;

	static constexpr uint32_t k_defaultLoaderWeight = 3;

	enum class LoadingState
	{
		LOADING_CURRENT,
		QUIT
	};

	//
	// Cosntruction and destuction.
	//
public:
	// Simple constructor.
	RoomMgr();
	// Simple desructor.
	~RoomMgr();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init(); 
	void _ready();

	//
	// Public interface.
	//
public:
	// Adds room into loading queue, if room is allready in the queue simply changes its weight.
	void LoadRoom(const godot::String& roomId, uint32_t weight = k_defaultLoaderWeight);
	// Removes room from existent rooms stack, and from loader queue (if not fully loaded yet).	
	void UnloadRoom(const godot::String& roomId);
	// Returns true if room loaded or exist in loading queue.
	bool IsRoomKnown(const godot::String& roomId);
	// Returns true if room is loaded in the room stack.
	bool IsRoomLoaded(const godot::String& roomId) const;
	// If room is loaded returns 1.0f, if not room or loader is exists, returns 0.0f, otherwise returns loader percent.
	real_t GetLoadPercent(const godot::String& roomId) const;
	// Returns pointer to the loaded room handler, if one is exist.
	RoomHandlerPtr GetRoomHandler(const godot::String& roomId) const;
	// Refreshes rooms list. All rooms not in the list is deleted, all rooms in the list but not loaded, added to loading queue
	void RefreshRoomList(const std::vector<godot::String>& requirdRooms);

	//
	// Private methods.
	//
private:
	// NOTE: all 'locked' arguments corresponds for usage of corresponding mutexes. 
	// Sets loading state.
	void SetState(LoadingState state, bool locked);
	// Returns loaded state.
	LoadingState GetState(bool locked) const;
	// Returns first loader in the queue
	LoaderPtr GetFirstLoader(bool locked) const;
	// Clears all loaders which are invalid (for some reason).
	void ClearInvalidLoaders(bool locked);
	// Clears all loaders with specific id, returns number of loaders cleared.
	std::size_t ClearLoader(const godot::String& roomId, bool locked);
	// Adds room handler to the stack (if one allready exists does nothing).
	void AddRoomHandler(RoomHandlerPtr handler, bool locked);
	// Used for gdscript, couse last can't handle handler.
	Room* GetRoomRaw(const godot::String& roomId) const;

	// Actual thread processing function.
	static void ProcessLoading(RoomMgr& mgr);

	//
	// Private members.
	//
private:
	// Separate thread for loading rooms.
	std::thread m_loadingThread;
	// State of loading (used for regulating loading thread).
	LoadingState m_state;

	// Conditional used to wakeing up thread if new loader is added.
	std::condition_variable m_loadersConditional;
	// Mutex used for controling loader queue.
	std::mutex mutable m_loadersMutex;
	// Mutex used to control the rooms stack.
	std::mutex mutable m_roomsMutex;

	// Queue of currently existing loaders.
	WeightetLoadersStack m_loadersQueue;
	// Stack of all already fully loaded rooms.
	RoomHandlersMap m_rooms;
};

} // namespace bwn
