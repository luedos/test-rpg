#pragma once

#include "utility/AutoSingleton.h"

namespace bwn
{

class GameState;
class LoadingState;
class GameplayState;

class GameMgr : public godot::Node, public bwn::AutoSingleton<GameMgr>
{
	GODOT_CLASS(GameMgr, godot::Node)

	//
	// Private aliasing.
	//
private:
	using StatePtr = shared_obj_ptr<GameState>;
	using StateStack = std::vector<StatePtr>;
	using StateInitMap = std::map<godot::String, godot::Ref<godot::PackedScene>>;

	//
	// Public constraction and destruction.
	//
public:
	// Constructor.
	GameMgr();
	// Destructor.
	~GameMgr();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init();
	void _ready();
	void _process(real_t delta);

	//
	// Public interface.
	//
public:
	// Creates state with GameMgr state resource cach.
	shared_obj_ptr<GameState> CreateState(const godot::String& stateId);
	// Creates state of specific type with GameMgr state resource cach.
	template<typename StateT>
	shared_obj_ptr<StateT> CreateStateAs(const godot::String& stateId);
	// Pushes state into stack. Returns true if all ok, and state was pushed.
	bool PushState(shared_obj_ptr<GameState> state);
	// Pushes state into stack, and clears resource of the stack. Returns true if all ok, and state was pushed.
	bool SetState(shared_obj_ptr<GameState> state);
	// Creates state with GameMgr state resource cach, and pushes it into stack. Returns true if all ok, and state was pushed.
	bool CreateAndPushState(const godot::String& stateId);
	// Creates state with GameMgr state resource cach, and sets it as the only one. Returns true if all ok, and state was pushed.
	bool CreateAndSetState(const godot::String& stateId);
	// Removes current state, and replaces it with top one from the stack. If state stack is empty, does nothing. 
	void PopState();
	// Returns current state.
	const GameState* GetCurrentState() const;
	// Returns current state.
	GameState* GetCurrentState();
	// Clears stack on the next frame.
	void ClearStateStack();
	// Loads and creates default loading state.
	shared_obj_ptr<LoadingState> CreateDefaultLoadingState();
	// Loads and creates default gameplay state.
	shared_obj_ptr<GameplayState> CreateDefaultGameplayState();
	// Loads and creates default main menu state.
	shared_obj_ptr<GameState> CreateDefaultMainMenuState();
	// Loadings to the default main menu state (for loading used default Loading state).
	void GoToMainMenu();
	// Loadings to the default gameplay state (for loading used default Loading state).
	void GoToGameplay();

	//
	// Private methods.
	//
private:
	// Loads resource state from the resource path.
	godot::Ref<godot::PackedScene> LoadState(const godot::String& stateId);

	//
	// Private members.
	//
private:
	// Map of the game states resources.
	StateInitMap m_statesInitMap;
	// Stack of the game states.
	StateStack m_statesStack;
	// Current state.
	StatePtr m_currentState;
	// Variable for the clearing state stack on the next frame.
	bool m_clearStackRequeired;

	//
	// Private cashed data.
	//
private:
	// Default resource key for main menu state.
	godot::String m_defaultMainMenuResId;
	// Default resource key for gameplay state.
	godot::String m_defaultGameplayResId;
	// Default resource key for loading state.
	godot::String m_defaultLoadingResId;
	// Seters and getters for main menu default res id.
	void SetDefaultMainMenuResId(godot::String resId);
	godot::String GetDefaultMainMenuResId() const;
	// Seters and getters for gameplay default res id.
	void SetDefaultGameplayResId(godot::String resId);
	godot::String GetDefaultGameplayResId() const;
	// Seters and getters for loading default res id.
	void SetDefaultLoadingResId(godot::String resId);
	godot::String GetDefaultLoadingResId() const;
};

template<typename StateT>
shared_obj_ptr<StateT> GameMgr::CreateStateAs(const godot::String& stateId)
{
#if defined(BWN_DEBUG)
	const std::wstring test = stateId.unicode_str();
#endif

	godot::Ref<godot::PackedScene> scene = this->LoadState(stateId);

	shared_obj_ptr<StateT> ret;

	if (scene.is_valid()) 
	{
		shared_obj_ptr<godot::Node> node{scene->instance()};
		ret = object_pointer_cast<StateT>(node);
	}	

	return ret;
}

} // namespace bwn