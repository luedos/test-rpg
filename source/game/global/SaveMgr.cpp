#include "pch.h"

#include "SaveMgr.h"


using namespace bwn;

SaveMgr::SaveMgr()
{}

SaveMgr::~SaveMgr()
{}

void SaveMgr::_register_methods()
{
	godot::register_property<SaveMgr, godot::String>("temp_current_room", &SaveMgr::temp_roomId, "");
}

void SaveMgr::_init()
{

}

void SaveMgr::_ready()
{

}

void SaveMgr::_process(real_t delta)
{

}

godot::String SaveMgr::GetCurrentRoomId() const
{
	return temp_roomId;
}
