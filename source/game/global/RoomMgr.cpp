#include "pch.h"

#include "utility/ObjectHolder.h"
#include "RoomMgr.h"
#include "level/rooms/RoomHandler.h"
#include "level/rooms/RoomAsyncLoader.h"

using namespace bwn;

RoomMgr::RoomMgr()
	: m_state{ LoadingState::LOADING_CURRENT }
{}

RoomMgr::~RoomMgr()
{
	SetState(LoadingState::QUIT, true);
	m_loadersConditional.notify_all();

	if (m_loadingThread.joinable())
	{
		m_loadingThread.join();
	}
}

void RoomMgr::_register_methods() 
{
	godot::register_method("_ready", &RoomMgr::_ready);
	godot::register_method("get_room", &RoomMgr::GetRoomRaw);
	godot::register_method("load_room", &RoomMgr::LoadRoom);
	godot::register_method("unload_room", &RoomMgr::UnloadRoom);
	godot::register_method("is_room_loaded", &RoomMgr::IsRoomLoaded);
	godot::register_method("get_load_percent", &RoomMgr::GetLoadPercent);
}

void RoomMgr::_init() 
{}

void RoomMgr::_ready()
{
	m_loadingThread = std::thread(&RoomMgr::ProcessLoading, std::ref<RoomMgr>(*this));
	
}

void RoomMgr::LoadRoom(const godot::String& roomId, uint32_t weight)
{
	BWN_ASSERT_WITH_MSG(!roomId.empty(), "Trying to load room with empty id!");
	if (roomId.empty()) {
		return;
	}

	BWN_LOG("Start to load room \"" << roomId.ascii().get_data() << "\"..");
	{	
		std::lock(m_loadersMutex, m_roomsMutex);
		std::lock_guard<std::mutex> lockLoader	(m_loadersMutex, std::adopt_lock);
		std::lock_guard<std::mutex> lockRes		(m_roomsMutex, std::adopt_lock);

		if (m_rooms.find(roomId) != m_rooms.end()) 
		{
			BWN_LOG("Room \"" << roomId.ascii().get_data() << "\" already loaded.");
			return;
		}

		WeightetLoadersStack::iterator loaderIt = std::find_if(
			m_loadersQueue.begin(), 
			m_loadersQueue.end(), 
			[&roomId](const WeightedAsyncLoader& loader)
			{
				return loader.second->GetId() == roomId; 
			});

		if (loaderIt != m_loadersQueue.end())
		{
			BWN_LOG("Room \"" << roomId.ascii().get_data() << "\" already in the loading queue.");
			loaderIt->first = weight;
		}
		else
		{
			RoomAsyncLoader loader{roomId};
			BWN_ASSERT_WITH_MSG(loader.IsValid(), "Can't create loader for the \"" << roomId.ascii().get_data() << "\" room.");
			if (loader.IsValid())
			{
				BWN_LOG("Room \"" << roomId.ascii().get_data() << "\" added to the loading queue.");
				m_loadersQueue.emplace_back(weight, std::make_shared<RoomAsyncLoader>(std::move(loader)));
			}
		}

		std::sort(
			m_loadersQueue.begin(), 
			m_loadersQueue.end(), 
			[](const WeightedAsyncLoader& l, const WeightedAsyncLoader& r)->bool
			{
				return l.first > r.first;
			});
	}

	m_loadersConditional.notify_all();
}
	
void RoomMgr::UnloadRoom(const godot::String& roomId)
{
	BWN_LOG("Unloading \"" << roomId.ascii().get_data() << "\" room..");
	ClearLoader(roomId, true);

	std::unique_lock<std::mutex> lock{m_roomsMutex};
	RoomHandlersMap::const_iterator roomIt = m_rooms.find(roomId);

	if (roomIt != m_rooms.end())
	{
		m_rooms.erase(roomIt);
		BWN_LOG("Room \"" << roomId.ascii().get_data() << "\" deleted from stack.");
	}
}

bool RoomMgr::IsRoomKnown(const godot::String& roomId)
{
	std::lock(m_loadersMutex, m_roomsMutex);
	std::lock_guard<std::mutex> lockLoader	(m_loadersMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockRoom	(m_roomsMutex, std::adopt_lock);

	return 	m_rooms.find(roomId) != m_rooms.cend() 
		|| std::find_if(m_loadersQueue.begin(), m_loadersQueue.end(), 
		[&roomId](const WeightedAsyncLoader& loader)
		{
			return loader.second && loader.second->GetId() == roomId;
		}) != m_loadersQueue.end(); 
}

bool RoomMgr::IsRoomLoaded(const godot::String& roomId) const
{
	std::unique_lock<std::mutex> lock{m_roomsMutex};

	return m_rooms.find(roomId) != m_rooms.cend();
}

real_t RoomMgr::GetLoadPercent(const godot::String& roomId) const
{
	{
		std::unique_lock<std::mutex> lock{m_roomsMutex};

		if (m_rooms.find(roomId) != m_rooms.end()) {
			return 1.0f;
		}
	}

	std::unique_lock<std::mutex> lock{m_loadersMutex};

	WeightetLoadersStack::const_iterator loaderIt = std::find_if(
		m_loadersQueue.begin(), 
		m_loadersQueue.end(), 
		[&roomId](const WeightedAsyncLoader& loader)
		{
			return loader.second->GetId() == roomId; 
		});

	if (loaderIt != m_loadersQueue.end())
	{
		return loaderIt->second->GetPollPercent();
	}

	return 0.0f;
}

RoomMgr::RoomHandlerPtr RoomMgr::GetRoomHandler(const godot::String& roomId) const
{
	std::unique_lock<std::mutex> lock{m_roomsMutex};

	RoomHandlersMap::const_iterator roomIt = m_rooms.find(roomId);

	const bool roomExist = roomIt != m_rooms.end();
	BWN_ASSERT_WITH_MSG(roomExist, "Room \"" << roomId.ascii().get_data() << "\" is not loaded.");
	if (roomExist)
	{
		return roomIt->second;
	}

	return RoomHandlerPtr{};
}

void RoomMgr::RefreshRoomList(const std::vector<godot::String>& requirdRooms)
{
	BWN_LOG("Refreshing rooms loading list..");	
	{
		std::lock(m_loadersMutex, m_roomsMutex);
		std::lock_guard<std::mutex> lockLoader	(m_loadersMutex, std::adopt_lock);
		std::lock_guard<std::mutex> lockRes		(m_roomsMutex, std::adopt_lock);

		const auto isInList = [&requirdRooms](const godot::String& roomId) -> bool
		{
			return std::find(requirdRooms.begin(), requirdRooms.end(), roomId) != requirdRooms.end();
		};

		// Deleting rooms not in the list.
		for (RoomHandlersMap::const_iterator roomIt = m_rooms.cbegin(); roomIt != m_rooms.cend();)
		{
			if (!isInList(roomIt->first))
			{
				BWN_LOG("Room \"" << roomIt->first.ascii().get_data() << "\" deleted from stack.");
				roomIt = m_rooms.erase(roomIt);
			}
			else
			{
				++roomIt;
			}
		}

		// Deleting loaders not in the list.
		for (WeightetLoadersStack::const_iterator loaderIt = m_loadersQueue.cbegin(); loaderIt != m_loadersQueue.cend();)
		{
			if (!loaderIt->second || !isInList(loaderIt->second->GetId()))
			{
				BWN_LOG("Room \"" << loaderIt->second->GetId().ascii().get_data() << "\" deleted from the loading queue.");
				loaderIt = m_loadersQueue.erase(loaderIt);
			}
			else
			{
				++loaderIt;
			}
		}

		// Adding everything absent from the list.
		for (const godot::String& roomId : requirdRooms)
		{
			// If room allready exist, do nothing.
			if (m_rooms.find(roomId) != m_rooms.end()) {
				continue;
			}

			// if loader allready exist, do nothing.
			if (std::find_if(m_loadersQueue.begin(), m_loadersQueue.end(), [&roomId](const WeightedAsyncLoader& loader)->bool
			{
				return loader.second && loader.second->GetId() == roomId;
			}) != m_loadersQueue.end())
			{
				continue;
			}

			// Adding new loader.
			RoomAsyncLoader loader{roomId};
			BWN_ASSERT_WITH_MSG(loader.IsValid(), "Can't create loader for the \"" << roomId.ascii().get_data() << "\" room.");
			if (loader.IsValid())
			{
				BWN_LOG("Room \"" << roomId.ascii().get_data() << "\" added to the loading queue.");
				m_loadersQueue.emplace_back(k_defaultLoaderWeight, std::make_shared<RoomAsyncLoader>(std::move(loader)));
			}
		}

		std::sort(
			m_loadersQueue.begin(), 
			m_loadersQueue.end(), 
			[](const WeightedAsyncLoader& l, const WeightedAsyncLoader& r)->bool
			{
				return l.first > r.first;
			});
	}
	m_loadersConditional.notify_all();

	BWN_LOG("Rooms loading list refreshed.");
}

void RoomMgr::SetState(LoadingState state, bool locked)
{
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_loadersMutex};
	}

	m_state = state;
}

RoomMgr::LoadingState RoomMgr::GetState(bool locked) const
{
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_loadersMutex};
	}

	return m_state;
}

RoomMgr::LoaderPtr RoomMgr::GetFirstLoader(bool locked) const
{
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_loadersMutex};
	}

	LoaderPtr ret;
	if (!m_loadersQueue.empty()) {
		ret = m_loadersQueue.front().second;
	}

	return ret;
}

void RoomMgr::ClearInvalidLoaders(bool locked)
{
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_loadersMutex};
	}

	m_loadersQueue.erase(
		std::remove_if(
			m_loadersQueue.begin(), 
			m_loadersQueue.end(), 
			[](const WeightedAsyncLoader& l)->bool
			{
				return !l.second || !l.second->IsValid();
			}), 
		m_loadersQueue.end());
}

std::size_t RoomMgr::ClearLoader(const godot::String& roomId, bool locked)
{
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_loadersMutex};
	}

	WeightetLoadersStack::const_iterator removedIt = std::remove_if(
		m_loadersQueue.begin(), 
		m_loadersQueue.end(), 
		[&roomId](const WeightedAsyncLoader& l)->bool
		{
			return l.second->GetId() == roomId;
		});

	const std::size_t removedCount = std::distance(removedIt, m_loadersQueue.cend());

	m_loadersQueue.erase(removedIt, m_loadersQueue.end());

	return removedCount;
}

void RoomMgr::AddRoomHandler(RoomHandlerPtr handler, bool locked)
{
	if (!handler || !handler->IsValid())
	{
		return;
	}
	
	std::unique_lock<std::mutex> lock;

	if (locked) {
		lock = std::unique_lock<std::mutex>{m_roomsMutex};
	}

	const bool found = m_rooms.find(handler->GetId()) != m_rooms.end();
	BWN_ASSERT_WITH_MSG(!found, "Loaded room \"" << handler->GetId().ascii().get_data() << "\" already exist.");
	if (!found)
	{
		BWN_LOG("New room \"" << handler->GetId().ascii().get_data() << "\" is loaded.");
		m_rooms[handler->GetId()] = handler;
	}	
}

Room* RoomMgr::GetRoomRaw(const godot::String& roomId) const
{
	RoomHandlerPtr handler = GetRoomHandler(roomId);

	if (handler)
	{
		return handler->GetRoom();
	}

	return nullptr;
}

void RoomMgr::ProcessLoading(RoomMgr& mgr)
{
	while (true)
	{
		const RoomMgr::LoadingState state = mgr.GetState(true);

		switch (state)
		{
		case RoomMgr::LoadingState::LOADING_CURRENT:
			{
				LoaderPtr loader;

				{
					std::unique_lock<std::mutex> lock{mgr.m_loadersMutex};
					loader = mgr.GetFirstLoader(false);
					
					if (!loader)
					{
						// no loader provided.
						mgr.m_loadersConditional.wait(lock);

						// overloading state status.
						break; 
					}
				}

				if (!loader->IsValid())
				{
					mgr.ClearInvalidLoaders(true);
					break;
				}

				if (loader->IsResLoaded())
				{
					const bool reloadSuccess = loader->ReloadRoom();
					BWN_ASSERT_WITH_MSG(reloadSuccess, "Loader \"" << loader->GetId().ascii().get_data() << "\" failed to reload the room.");
					if (!reloadSuccess) 
					{
						mgr.ClearLoader(loader->GetId(), true);
						break;
					}

					RoomHandlerPtr handler = std::make_shared<RoomHandler>(loader->ExtractRoomHandler());

					// in case we cleared loader in mgr, while we were instanciating room here, we need to check if loader is still in queue.
					std::unique_lock<std::mutex> lock{mgr.m_loadersMutex};
					if (mgr.ClearLoader(loader->GetId(), false) != 0)
					{
						mgr.AddRoomHandler(std::move(handler), true);
					}

					break;
				}

				const godot::Error error = loader->PollResource();
				
				BWN_ASSERT_WITH_MSG((error == godot::Error::OK || error == godot::Error::ERR_FILE_EOF), "Error loading room \"" << loader->GetId().ascii().get_data() << "\".");
			}
			break;

		case RoomMgr::LoadingState::QUIT:
			goto quit;
		}
	}
	quit:
	{}
}