#include "pch.h"

#include "GameMgr.h"

#include "SaveMgr.h"

#include "game_states/GameState.h"
#include "game_states/LoadingState.h"
#include "game_states/GameplayState.h"

#include "sequences/LoadingGameplaySeq.h"
#include "sequences/LoadingMainMenuSeq.h"

#include "utility/PackedSceneHelper.h"

using namespace bwn;

GameMgr::GameMgr()
	: m_statesInitMap{}
	, m_statesStack{}
	, m_currentState{}
	, m_clearStackRequeired{false}
{}

GameMgr::~GameMgr() = default;

void GameMgr::_register_methods()
{
	godot::register_method("_ready", &GameMgr::_ready);
	godot::register_method("_process", &GameMgr::_process);

	godot::register_method("create_and_push_state", &GameMgr::CreateAndPushState);
	godot::register_method("create_and_set_state", &GameMgr::CreateAndSetState);
	godot::register_method("pop_state", &GameMgr::PopState);
	godot::register_method<GameState*(GameMgr::*)()>("get_current_state", &GameMgr::GetCurrentState);
	godot::register_method("clear_state_stack", &GameMgr::ClearStateStack);

	godot::register_method("goto_main_menu", &GameMgr::GoToMainMenu);
	godot::register_method("goto_gameplay", &GameMgr::GoToGameplay);

	godot::register_property<GameMgr, godot::String>(
		"default_loading_state", 
		&GameMgr::SetDefaultLoadingResId, 
		&GameMgr::GetDefaultLoadingResId, 
		godot::String(""));
	godot::register_property<GameMgr, godot::String>(
		"default_gameplay_state", 
		&GameMgr::SetDefaultGameplayResId, 
		&GameMgr::GetDefaultGameplayResId, 
		godot::String(""));
	godot::register_property<GameMgr, godot::String>(
		"default_mainmenu_state", 
		&GameMgr::SetDefaultMainMenuResId, 
		&GameMgr::GetDefaultMainMenuResId, 
		godot::String(""));
}

void GameMgr::_init()
{}

void GameMgr::_ready()
{}

void GameMgr::_process(real_t delta)
{
	if (!m_currentState.is_valid())
	{
		GameState*const state = cast_to<GameState>(get_tree()->get_current_scene());
		if (state != nullptr)
		{
			BWN_LOG("Current state is updated as \"" << state->get_name().ascii().get_data() << "\" (got from scene).");
			m_currentState = shared_obj_ptr<GameState>(state);
		}
	}

	if (m_clearStackRequeired)
	{
		m_clearStackRequeired = false;
		m_statesStack.clear();
		BWN_LOG("State stack is cleared.");
	}
}

shared_obj_ptr<GameState> GameMgr::CreateState(const godot::String& stateId)
{
	godot::Ref<godot::PackedScene> res = LoadState(stateId);
	BWN_ASSERT_WITH_MSG(res.is_valid(), "Error creating game state from resource \"" << stateId.ascii().get_data() << "\", resource not loaded.");
	if (!res.is_valid()) {
		return false;
	}

	shared_obj_ptr<GameState> state = object_pointer_cast<GameState>(shared_obj_ptr<godot::Node>(res->instance()));
	BWN_ASSERT_WITH_MSG(res.is_valid(), "Error creating game state from resource \"" << stateId.ascii().get_data() << "\", instance cast error.");
	if (!state.is_valid()) {
		return false;
	}

	return state;
}

bool GameMgr::PushState(shared_obj_ptr<GameState> state)
{
	BWN_ASSERT_WITH_MSG(state.is_valid(), "Error pushing game state, state is nullptr");
	if (!state.is_valid()) {
		return false;
	}

	godot::Viewport*const root = get_tree()->get_root();

	if (m_currentState.is_valid()) 
	{		
		const bool currentNodeExist = root->is_a_parent_of(m_currentState.get_object());
		BWN_ASSERT_WITH_MSG(currentNodeExist, "Current game state \"" << m_currentState.get_object()->get_name().ascii().get_data() << "\" is not setuped in the root");
		if (currentNodeExist) {
			root->remove_child(m_currentState.get_object());
		}
		m_statesStack.push_back(std::move(m_currentState));
	}

	m_currentState = std::move(state);
	root->add_child(m_currentState.get_object());
	
	BWN_LOG("New game state pushed: \"" << m_currentState.get_object()->get_name().ascii().get_data() << "\".");

	return true;
}

bool GameMgr::SetState(shared_obj_ptr<GameState> state)
{
	if (!PushState(std::move(state))) {
		return false;
	}

	ClearStateStack();
	return true;
}

bool GameMgr::CreateAndPushState(const godot::String& stateId)
{
	return PushState(CreateState(stateId));
}

bool GameMgr::CreateAndSetState(const godot::String& stateId)
{
	return SetState(CreateState(stateId));
}

void GameMgr::PopState()
{	
	shared_obj_ptr<GameState> state;

	while(!m_statesStack.empty())
	{
		if (m_statesStack.back().is_valid())
		{
			state = m_statesStack.back();
			m_statesStack.pop_back();
			break;
		}
		else
		{
			m_statesStack.pop_back();
		}		
	}

	if (!state.is_valid()) 
	{
		BWN_LOG("Pop game state, no new state available.");
		return;
	}

	m_currentState = std::move(state);
	get_tree()->set_current_scene(m_currentState.get_object());

	BWN_LOG("Pop game state, new state is: \"" << m_currentState.get_object()->get_name().ascii().get_data() << "\".");
}

const GameState* GameMgr::GetCurrentState() const
{
	return m_currentState.get_object();
}

GameState* GameMgr::GetCurrentState()
{
	return m_currentState.get_object();
}

void GameMgr::ClearStateStack()
{
	m_clearStackRequeired = true;
}

shared_obj_ptr<LoadingState> GameMgr::CreateDefaultLoadingState()
{
	shared_obj_ptr<LoadingState> state = CreateStateAs<LoadingState>(m_defaultLoadingResId);
	BWN_ASSERT_WITH_MSG(state.is_valid(), "Can't create default LoadingState \"" << m_defaultLoadingResId.ascii().get_data() <<"\".");
	return state;
}

shared_obj_ptr<GameplayState> GameMgr::CreateDefaultGameplayState()
{	
	shared_obj_ptr<GameplayState> state = CreateStateAs<GameplayState>(m_defaultGameplayResId);
	BWN_ASSERT_WITH_MSG(state.is_valid(), "Can't create default GameplayState \"" << m_defaultGameplayResId.ascii().get_data() <<"\".");
	
	return state;
}

shared_obj_ptr<GameState> GameMgr::CreateDefaultMainMenuState()
{
	shared_obj_ptr<GameState> state = CreateStateAs<GameState>(m_defaultMainMenuResId);
	BWN_ASSERT_WITH_MSG(state.is_valid(), "Can't create default MainMenu State \"" << m_defaultMainMenuResId.ascii().get_data() <<"\".");
	
	return state;
}

void GameMgr::GoToMainMenu()
{
	BWN_LOG("Going to the Main Menu.");
	shared_obj_ptr<LoadingState> loadingState = CreateDefaultLoadingState();

	if (!loadingState.is_valid()) {
		return;
	}

	shared_obj_ptr<GameState> mainMenuState = CreateDefaultMainMenuState();
	
	if (!mainMenuState.is_valid()) {
		return;
	}

	loadingState.get_object()->SetupLoadingState(static_pointer_cast<GameState>(mainMenuState));

	{
		std::unique_ptr<LoadingMainMenuSeq> seq = std::make_unique<LoadingMainMenuSeq>();
		loadingState.get_object()->AddSequnce(std::move(seq));
	}

	PushState(static_pointer_cast<GameState>(loadingState));
}

void GameMgr::GoToGameplay()
{
	BWN_LOG("Going to the Gameplay.");
	shared_obj_ptr<LoadingState> loadingState = CreateDefaultLoadingState();

	if (!loadingState.is_valid()) {
		return;
	}

	shared_obj_ptr<GameplayState> gameplayState = CreateDefaultGameplayState();
	
	if (!gameplayState.is_valid()) {
		return;
	}

	loadingState.get_object()->SetupLoadingState(static_pointer_cast<GameState>(gameplayState));

	{
		std::unique_ptr<LoadingGameplaySeq> seq = std::make_unique<LoadingGameplaySeq>(gameplayState);
		seq->SetRoomIds({ SaveMgr::GetInstance()->GetCurrentRoomId() });
		loadingState.get_object()->AddSequnce(std::move(seq));
	}

	PushState(static_pointer_cast<GameState>(loadingState));
}


godot::Ref<godot::PackedScene> GameMgr::LoadState(const godot::String& stateId)
{
	godot::Ref<godot::PackedScene> ret;
	
	do
	{
		{
			StateInitMap::iterator resIt = m_statesInitMap.find(stateId);

			if (resIt != m_statesInitMap.end() && resIt->second.is_valid())
			{
				ret = resIt->second;
				break;
			}
		}

		godot::Ref<godot::PackedScene> generalRes = godot::ResourceLoader::get_singleton()->load(stateId);
		godot::Ref<godot::PackedScene> packedSceneRes = godot::Object::cast_to<godot::PackedScene>(generalRes.ptr());

		const bool sceneCorrect = utility::PackedSceneHelper(packedSceneRes).IsNativeScriptOfType<GameState>();	
		if (!sceneCorrect)
		{
			break;
		}		

		ret = packedSceneRes;
		// intentionally replaces scene if one exist in the map, because if we in this section of code, this res is invalid.
		m_statesInitMap[stateId] = packedSceneRes;

	} while(false);

	BWN_ASSERT_WITH_MSG(ret.is_valid(), "Can't load game state \"" << stateId.ascii().get_data() << "\", because of incorect resource, or script attached to the root node.");
	return ret;
}

void GameMgr::SetDefaultMainMenuResId(godot::String resId)
{
	m_defaultMainMenuResId = resId;
	const bool success = LoadState(resId).is_valid();
	BWN_ASSERT_WITH_MSG(success, "Failed loading default main menu state \"" << resId.ascii().get_data() << "\".");
}

godot::String GameMgr::GetDefaultMainMenuResId() const
{
	return m_defaultMainMenuResId;
}

void GameMgr::SetDefaultGameplayResId(godot::String resId)
{
	m_defaultGameplayResId = resId;
	const bool success = LoadState(resId).is_valid();
	BWN_ASSERT_WITH_MSG(success, "Failed loading default gameplay state \"" << resId.ascii().get_data() << "\".");
}

godot::String GameMgr::GetDefaultGameplayResId() const
{
	return m_defaultGameplayResId;
}

void GameMgr::SetDefaultLoadingResId(godot::String resId)
{
	m_defaultLoadingResId = resId;
	const bool success = LoadState(resId).is_valid();
	BWN_ASSERT_WITH_MSG(success, "Failed loading default loading state \"" << resId.ascii().get_data() << "\".");
}

godot::String GameMgr::GetDefaultLoadingResId() const
{
	return m_defaultLoadingResId;
}
