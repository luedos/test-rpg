#include "pch.h"

#include "Character.h"
#include <ClassDB.hpp>
using namespace bwn;

const char*const Character::k_upAction = "m_u";
const char*const Character::k_downAction = "m_d";
const char*const Character::k_rightAction = "m_r";
const char*const Character::k_leftAction = "m_l";

Character::Character() 
	: m_horMoveActions{ k_rightAction, k_leftAction }
	, m_verMoveActions{ k_downAction, k_upAction }
	, m_inputMoveDirection{}
	, m_velocity{}
	, m_maxSpeed{ 100.0f }
	, m_moveAcceleration{ 400.0f }
	, m_hasMoveInput{ false }
{}

Character::~Character()
{
	godot::Godot::print("Actual character destructor");
}

void Character::_register_methods() 
{
	godot::register_method("_notification", &Character::_notification);
	godot::register_method("_process", &Character::_process);
	godot::register_method("_ready", &Character::_ready);
	godot::register_method<godot::Vector2(Character::*)()const>("get_velocity", &Character::GetVelocity);

	godot::register_property<Character, real_t>("max_speed", &Character::SetMaxSpeed, &Character::GetMaxSpeed, 100.0f);
	godot::register_property<Character, real_t>("move_acceleration", &Character::SetMoveAcceleration, &Character::GetMoveAcceleration, 400.0f);
}

void Character::_init() 
{}

void Character::_ready()
{}

void Character::_process(real_t delta) 
{
	/*
	static real_t s_deathTimer = 0.0f;
	static int32_t s_deathCounter = 0;
	s_deathTimer += delta;
	const int32_t newDeathCounter = static_cast<int32_t>(s_deathTimer / 5.0f);
	if (newDeathCounter > s_deathCounter)
	{
		s_deathCounter = newDeathCounter;
		
		godot_string_name class_name;
		godot::api->godot_string_name_new_data(&class_name, ___get_type_name());

		godot::Godot::print("internal type_tag " + godot::String::num_int64(static_cast<int64_t>(_type_tag)));
		godot::Godot::print("type tag from name " + godot::String::num_int64(*reinterpret_cast<const int64_t*>(&class_name._dont_touch_that)) + " : " + godot::String::num_int64(
			(int64_t)godot::core_1_2_api->godot_get_class_tag(
				//godot::_RegisterState::language_index, 
				&class_name)));

		godot::Godot::print("type global tag " + godot::String::num_int64(
			(std::size_t)godot::nativescript_1_1_api->godot_nativescript_get_global_type_tag(
				godot::_RegisterState::language_index, ___get_type_name())));

		godot::Godot::print("type tag from owner " + godot::String::num_int64(
			(int64_t)godot::nativescript_1_1_api->godot_nativescript_get_type_tag(_owner)));

		godot::api->godot_string_name_destroy(&class_name);

		godot::Godot::print("GODOT_CLASS type_tag " + godot::String::num_int64(static_cast<int64_t>(___get_id())));
	
		godot::Godot::print("just some");
	}
	*/
	//godot::Godot::print("Character _process: " + godot::String::num_real(delta));
	UpdateInput();	

	m_velocity = m_velocity.move_toward(m_inputMoveDirection * m_maxSpeed, m_moveAcceleration * delta);
	m_velocity = move_and_slide(m_velocity);
}

void Character::_notification(int64_t what)
{}

void Character::SetMovementInput(std::int32_t horizontal, std::int32_t vertical)
{
	m_inputMoveDirection = godot::Vector2(
		static_cast<real_t>(godot::Math::clamp(horizontal, -1, 1)),
		static_cast<real_t>(godot::Math::clamp(vertical, -1, 1))
	).normalized();
	m_hasMoveInput = horizontal != 0 && vertical != 0;
}

godot::Vector2 Character::GetVelocity() const
{
	return m_velocity;
}

real_t Character::GetMaxSpeed() const
{
	return m_maxSpeed;
}

void Character::SetMaxSpeed(real_t speed)
{
	m_maxSpeed = speed;
}

real_t Character::GetMoveAcceleration() const
{
	return m_moveAcceleration;
}
void Character::SetMoveAcceleration(real_t acceleration)
{
	m_moveAcceleration = acceleration;
}


void Character::BundeledActions::UpdateInput()
{
	const godot::Input*const input = godot::Input::get_singleton();

	const bool posPressed = m_posAction != nullptr && input->is_action_pressed(m_posAction);
	const bool negPressed = m_negAction != nullptr && input->is_action_pressed(m_negAction);
	
	if (posPressed && negPressed)
	{
		const bool posJustPressed = m_posAction != nullptr && input->is_action_just_pressed(m_posAction);
		const bool negJustPressed = m_negAction != nullptr && input->is_action_just_pressed(m_negAction);

		if (posJustPressed) {
			m_dir = 1;
		}
		if (negJustPressed) {
			m_dir = -1;
		}
	}
	else
	{
		m_dir = static_cast<int32_t>(posPressed) - static_cast<int32_t>(negPressed);
	}
	
	if (m_dir == 0)
	{
		m_action = 0.0f;
		return;
	}

	m_action = m_dir > 0
	? input->get_action_strength(m_posAction)
	: input->get_action_strength(m_negAction);
}

void Character::UpdateInput()
{
	m_horMoveActions.UpdateInput();
	m_verMoveActions.UpdateInput();

	SetMovementInput(m_horMoveActions.GetDir(), m_verMoveActions.GetDir());
}