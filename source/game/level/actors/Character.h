#pragma once

namespace bwn 
{

class Character : public godot::KinematicBody2D 
{
	GODOT_CLASS(Character, godot::KinematicBody2D)

public:
	Character();
	~Character();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init(); 
	void _ready();
	void _process(real_t delta);
	void _notification(const int64_t what);

	void Char_test(godot::Node* node);

	//
	// Public interface.
	//
public:
	void SetMovementInput( std::int32_t horizontal, std::int32_t vertical);

	godot::Vector2 GetVelocity() const;
	real_t GetMaxSpeed() const;
	void SetMaxSpeed(real_t speed);

	real_t GetMoveAcceleration() const;
	void SetMoveAcceleration(real_t acceleration);
	
	real_t GetMoveFriction() const;

	void PublicTestFunc() const;

	//
	// Private members.
	//
private:
	struct BundeledActions
	{
		BundeledActions(const char* posAction, const char* negAction)
			: m_posAction{ posAction }
			, m_negAction{ negAction }
			, m_action{ 0.0f }
			, m_dir{ 0 }
		{}

		void UpdateInput();
		int32_t GetDir() const { return m_dir; }
		real_t GetAction() const { return m_action; }

	private:
		int32_t m_dir;
		real_t m_action; 
		const char* m_posAction;
		const char* m_negAction;
	};

	void UpdateInput();

	BundeledActions m_horMoveActions;
	BundeledActions m_verMoveActions;

	godot::Vector2 m_inputMoveDirection;
	godot::Vector2 m_velocity;
	float m_maxSpeed;
	float m_moveAcceleration;

	bool m_hasMoveInput;

	static const char*const k_upAction;
	static const char*const k_downAction;
	static const char*const k_rightAction;
	static const char*const k_leftAction;
};

}
