#include "pch.h"

#include "RoomHandler.h"
#include "Room.h"
#include "RoomExit.h"

#include <PackedScene.hpp>

using namespace bwn;

RoomHandler::RoomHandler(
	const godot::String& roomId, 
	godot::Ref<godot::PackedScene> res, 
	shared_obj_ptr<Room> node)
	: m_roomId{ roomId }
	, m_res{ res }
	, m_node{ node }
{
	if (m_roomId.empty() || !m_res.is_valid() || !m_node.is_valid())
	{
		m_roomId = godot::String{};
		m_res.unref();
		m_node.reset();
	}
	else
	{
		ReculcExits();
	}
}

RoomHandler::RoomHandler() = default;
RoomHandler::~RoomHandler() = default;

Room* RoomHandler::GetRoom()
{
	return m_node.get_object();
}

const Room* RoomHandler::GetRoom() const
{
	return m_node.get_object();
}

const godot::String& RoomHandler::GetId() const
{
	return m_roomId;
}

const std::map<godot::String, ObjectHolder<RoomExit>>& RoomHandler::GetExits() const
{
	return m_exits;
}

bool RoomHandler::IsValid() const
{
	return !m_roomId.empty() && m_res.is_valid() && m_node.is_valid();
}

bool RoomHandler::IsRoomValid() const
{
	return m_node.is_valid();
}

bool RoomHandler::IsResourceValid() const
{
	return m_res.is_valid();
}

void RoomHandler::AppendNeighbors(std::vector<godot::String>& ids) const
{
	for (const auto& exitPair : m_exits)
	{
		const RoomExit*const exit = exitPair.second.GetObject();
		if (exit != nullptr)
		{
			ids.push_back(exit->GetDestRoomId());
		}
	}
}

void RoomHandler::ReculcExits()
{
	m_exits.clear();

	Room*const room = m_node.get_object();

	if (room == nullptr) {
		return;
	}

	AppendExits("", *room);
}

void RoomHandler::AppendExits(const godot::String& path, const godot::Node& node)
{
	const int64_t count = node.get_child_count();
	for (int64_t i = 0; i < count; ++i)
	{
		godot::Node*const child = node.get_child(i);

		if (child == nullptr) {
			continue;
		}

		const godot::String childPath = path + "/" + child->get_name();
		RoomExit*const exit = godot::Object::cast_to<RoomExit>(child);

		if (exit != nullptr)
		{
			// We are not searching further. We assuming one exit cant hold another.			
			m_exits.emplace(childPath, exit);
		}
		else
		{
			AppendExits(childPath, *child);
		}
	}
}

