#include "pch.h"

#include "RoomExit.h"

#include "sequences/ChangingRoomSequence.h"
#include "game_states/GameplayState.h"

using namespace bwn;

RoomExit::RoomExit()
{}

RoomExit::~RoomExit() = default;

void RoomExit::_register_methods() 
{
	godot::register_method("start_room_changing", &RoomExit::StartRoomChanging);

	godot::register_property<RoomExit, godot::String>("dest_room_id", &RoomExit::m_destRoomId, "");
	godot::register_property<RoomExit, godot::String>("dest_exit_path", &RoomExit::m_destExitPath, "");
}

void RoomExit::_init()
{}

void RoomExit::_process(real_t delta)
{}

void RoomExit::StartRoomChanging() const
{
	// We can only perform room change on the gameplay state, because of this we are not going to get state from GameMgr, but from gameplay directly.
	GameplayState*const state = GameplayState::GetInstance();

	BWN_ASSERT_WITH_MSG(state != nullptr, "Can't change room if currently not in the gameplay state.");
	if (state == nullptr) {
		return;
	}

	std::unique_ptr<ChangingRoomSequence> seq = std::make_unique<ChangingRoomSequence>(state, m_destRoomId, m_destExitPath);
	// Just to be sure in the future.
	seq->SetTransitionMenu(state->GetRoomTransitionMenu());

	BWN_LOG("Room change initiated!");
	state->AddSequnce(std::move(seq));
}

const godot::String& RoomExit::GetDestRoomId() const
{
	return m_destRoomId;
}
const godot::String& RoomExit::GetDestExitPath() const
{
	return m_destExitPath;
}