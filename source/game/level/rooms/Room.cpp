#include "pch.h"

#include "Room.h"

using namespace bwn;

Room::Room()
{}

Room::~Room() = default;

void Room::_register_methods() 
{
}

void Room::_init() 
{}

void Room::_process(real_t delta) 
{
}