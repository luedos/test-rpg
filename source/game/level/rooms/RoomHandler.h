#pragma once

namespace godot
{
class Node;
class PackedScene;
}

namespace bwn 
{

class Room;
class RoomExit;
class RoomAsyncLoader;

class RoomHandler 
{
	friend RoomAsyncLoader;

	//
	// Construction and destruction.
	//
public:
	// Destructor (will destory room node if needed)
	RoomHandler();
	~RoomHandler();
	
	RoomHandler(const RoomHandler&) = default;
	RoomHandler(RoomHandler&&) noexcept = default;

	RoomHandler& operator=(const RoomHandler&) = default;
	RoomHandler& operator=(RoomHandler&&) noexcept = default;

	//
	// Private constuction.
	//
private:
	RoomHandler(const godot::String& roomId, godot::Ref<godot::PackedScene> res, shared_obj_ptr<Room> node);

	//
	// Public interface.
	//
public:
	// Returns room node. (if room is not loaded or valid, reutnrs nullptr)
	Room* GetRoom();
	// Returns room node. (if room is not loaded or valid, reutnrs nullptr)
	const Room* GetRoom() const;
	// Returns room id.
	const godot::String& GetId() const; 
	// Returns all the exits from room.
	const std::map<godot::String, ObjectHolder<RoomExit>>& GetExits() const;
	// Returns true if room is valid (Node and Res is loaded, room Id is not empty).
	bool IsValid() const;
	// Returns true if room node is valid.
	bool IsRoomValid() const;
	// Returns true if room resource is valid.
	bool IsResourceValid() const;
	// Appends all neighbors ids to vector.
	void AppendNeighbors(std::vector<godot::String>& ids) const;

	//
	// Private methods.
	//
private:
	// Resets neighbors with current node exits.
	void ReculcExits();
	// Adds Exit nodes to the m_exits value recursivly. Path is relative path to the node.
	void AppendExits(const godot::String& path, const godot::Node& node);

	//
	// Private members.
	//
private:
	// Actual room id (currently is resurce path).
	godot::String m_roomId;
	// Room resource (not really used for now).
	godot::Ref<godot::PackedScene> m_res;
	// Actual room node.
	shared_obj_ptr<Room> m_node;
	// All room exits.
	std::map<godot::String, ObjectHolder<RoomExit>> m_exits;
};

} // namespace bwn.
