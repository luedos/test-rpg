#pragma once

namespace godot
{
class StaticBody2D;
}

namespace bwn 
{

class RoomExit : public godot::StaticBody2D 
{
	GODOT_CLASS(RoomExit, godot::StaticBody2D)

public:
	RoomExit();
	~RoomExit();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init(); 
	void _process(real_t delta);

	//
	// Public interface.
	//
public:
	// Pushes respected sequence to the game state, to chage room.
	void StartRoomChanging() const;
	// Returns id of the destination room.
	const godot::String& GetDestRoomId() const;
	// Returns path of the exit object in the destination room.
	const godot::String& GetDestExitPath() const;


	//
	// Private members.
	//
private:
	godot::String m_destRoomId;
	godot::String m_destExitPath;
};

}
