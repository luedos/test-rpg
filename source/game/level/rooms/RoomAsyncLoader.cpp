#include "pch.h"

#include "RoomAsyncLoader.h"
#include "Room.h"
#include "RoomHandler.h"

#include "utility/PackedSceneHelper.h"

using namespace bwn;


RoomAsyncLoader::RoomAsyncLoader()
	: m_roomId{}
	, m_loader{}
	, m_res{}
	, m_node{}
	, m_loadPrecent{ 0.0f }
	, m_resMutex{}
	, m_nodeMutex{}
	, m_loaderMutex{}
	, m_loadPrecentMutex{}
{}

RoomAsyncLoader::RoomAsyncLoader(const godot::String& roomId)
{
	ResetRoom(roomId);
}

RoomAsyncLoader::~RoomAsyncLoader()
{
	
}

RoomAsyncLoader::RoomAsyncLoader(RoomAsyncLoader&& other) noexcept
{
	std::lock(
		m_loaderMutex, 
		m_resMutex, 
		m_nodeMutex, 
		m_loadPrecentMutex,
		other.m_loaderMutex, 
		other.m_resMutex, 
		other.m_nodeMutex, 
		other.m_loadPrecentMutex);
	std::lock_guard<std::mutex> lockLoader        (m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockRes           (m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockNode          (m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockPercent       (m_loadPrecentMutex, std::adopt_lock);

	std::lock_guard<std::mutex> lockOtherLoader   (other.m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherRes      (other.m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherNode     (other.m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherPercent  (other.m_loadPrecentMutex, std::adopt_lock);

	m_loader = std::move(other.m_loader);
	m_res = std::move(other.m_res);
	m_node = std::move(other.m_node);
	m_roomId = other.m_roomId;
	other.m_roomId = godot::String();
	m_loadPrecent = other.m_loadPrecent;
	other.m_loadPrecent = 0.0f;
}

RoomAsyncLoader& RoomAsyncLoader::operator=(RoomAsyncLoader&& other) noexcept
{
	std::lock(
		m_loaderMutex, 
		m_resMutex, 
		m_nodeMutex, 
		m_loadPrecentMutex,
		other.m_loaderMutex, 
		other.m_resMutex, 
		other.m_nodeMutex, 
		other.m_loadPrecentMutex);
	std::lock_guard<std::mutex> lockLoader        (m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockRes           (m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockNode          (m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockPercent       (m_loadPrecentMutex, std::adopt_lock);

	std::lock_guard<std::mutex> lockOtherLoader   (other.m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherRes      (other.m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherNode     (other.m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockOtherPercent  (other.m_loadPrecentMutex, std::adopt_lock);

	m_loader = std::move(other.m_loader);
	m_res = std::move(other.m_res);
	m_node = std::move(other.m_node);
	m_roomId = other.m_roomId;
	m_loadPrecent = other.m_loadPrecent;
	other.m_loadPrecent = 0.0f;

	return *this;
}


godot::String RoomAsyncLoader::GetId() const
{
	std::unique_lock<std::mutex> lock{m_nodeMutex};
	return m_roomId;
}

void RoomAsyncLoader::ResetRoom(const godot::String& roomId)
{
	std::lock(m_loaderMutex, m_resMutex, m_nodeMutex, m_loadPrecentMutex);
	std::lock_guard<std::mutex> lockLoader	(m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockRes		(m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockNode	(m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockPercent	(m_loadPrecentMutex, std::adopt_lock);

	m_roomId = roomId;
	m_loader = godot::ResourceLoader::get_singleton()->load_interactive(m_roomId);

	m_loadPrecent = 0.0f;
	m_res.unref();
	m_node.reset();
}

godot::Error RoomAsyncLoader::PollResource()
{
	const std::unique_lock<std::mutex> lockLoader{m_loaderMutex};

	if (!m_loader.is_valid()) {
		return godot::Error::ERR_UNCONFIGURED;
	}

	const godot::Error error = m_loader->poll();

	{
		const std::unique_lock<std::mutex> lockPercent{m_loadPrecentMutex};
		m_loadPrecent = static_cast<real_t>(m_loader->get_stage()) / static_cast<real_t>(m_loader->get_stage_count());	
	}

	if (error == godot::Error::ERR_FILE_EOF)
	{
		godot::Ref<godot::PackedScene> generalRes = m_loader->get_resource();
		godot::Ref<godot::PackedScene> packedScene = godot::Object::cast_to<godot::PackedScene>(generalRes.ptr());

		const bool correctScene = utility::PackedSceneHelper(packedScene).IsNativeScriptOfType<Room>();
		if (!correctScene)
		{
			return godot::Error::ERR_INVALID_DATA;
		}

		const std::unique_lock<std::mutex> lockRes{m_resMutex};		
		m_res = packedScene;		
	}

	return error;
}

real_t RoomAsyncLoader::GetPollPercent() const
{
	const std::unique_lock<std::mutex> lock{m_loadPrecentMutex};
	return m_loadPrecent;
}

godot::Error RoomAsyncLoader::WaitResource()
{
	const std::unique_lock<std::mutex> lockLoader{m_loaderMutex};

	if (!m_loader.is_valid()) {
		return godot::Error::ERR_UNCONFIGURED;
	}

	const godot::Error error = m_loader->wait();

	{
		const std::unique_lock<std::mutex> lockPercent{m_loadPrecentMutex};
		m_loadPrecent = static_cast<real_t>(m_loader->get_stage()) / static_cast<real_t>(m_loader->get_stage_count());	
	}

	if (error == godot::Error::ERR_FILE_EOF)
	{
		godot::Ref<godot::PackedScene> generalRes = m_loader->get_resource();
		godot::Ref<godot::PackedScene> packedScene = godot::Object::cast_to<godot::PackedScene>(generalRes.ptr());

		const bool correctScene = utility::PackedSceneHelper(packedScene).IsNativeScriptOfType<Room>();
		if (!correctScene)
		{
			return godot::Error::ERR_INVALID_DATA;
		}

		const std::unique_lock<std::mutex> lockRes{m_resMutex};		
		m_res = packedScene;		
	}

	return error;
}

bool RoomAsyncLoader::ReloadRoom()
{
	const std::unique_lock<std::mutex> lockRes{m_resMutex};

	if (!m_res.is_valid()) {
		return false;
	}

	const shared_obj_ptr<godot::Node> node = shared_obj_ptr<godot::Node>(m_res->instance()); 

	const std::unique_lock<std::mutex> lockNode{m_nodeMutex};
	// If for some reason cast of nodes is ends up failing the original node will be deleted because of shared ptr.
	m_node = object_pointer_cast<Room>(node);

	return m_node.is_valid();
}

bool RoomAsyncLoader::IsResLoaded() const
{
	const std::unique_lock<std::mutex> lock{m_resMutex};
	return m_res.is_valid();
}

bool RoomAsyncLoader::IsNodeLoaded() const
{
	const std::unique_lock<std::mutex> lock{m_nodeMutex};
	return m_node.is_valid();
}

RoomHandler RoomAsyncLoader::ExtractRoomHandler()
{
	std::lock(m_loaderMutex, m_resMutex, m_nodeMutex, m_loadPrecentMutex);
	std::lock_guard<std::mutex> lockLoader	(m_loaderMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockRes		(m_resMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockNode	(m_nodeMutex, std::adopt_lock);
	std::lock_guard<std::mutex> lockPercent	(m_loadPrecentMutex, std::adopt_lock);

	RoomHandler ret(m_roomId, m_res, m_node);

	m_loader.unref();
	m_res.unref();
	m_node.reset();
	m_loadPrecent = 0.0f;

	return ret;
}

bool RoomAsyncLoader::IsValid() const
{
	std::unique_lock<std::mutex> lock{m_loaderMutex};

	return m_loader.is_valid();
}