#pragma once

#include "utility/shared_obj_ptr.h"

namespace bwn
{

class Room;
class RoomHandler;

class RoomAsyncLoader
{
	//
	// Construction and destruction.
	//
public:
	// Default constructor.
	RoomAsyncLoader();
	// Constructor with initial room id.
	explicit RoomAsyncLoader(const godot::String& roomId);

	// Destructor.
	~RoomAsyncLoader();

	// Because we mostly working on resorces, we need only one loader at a time.
	// Removed copy constructor.
	RoomAsyncLoader(RoomAsyncLoader&) = delete;
	// Removed copy operator.
	RoomAsyncLoader& operator=(RoomAsyncLoader&) = delete;

	// Move constructor.
	RoomAsyncLoader(RoomAsyncLoader&& other) noexcept;
	// Move operator.
	RoomAsyncLoader& operator=(RoomAsyncLoader&& other) noexcept;

	//
	// Public interface.
	//
public:
	// Returns id.
	godot::String GetId() const;
	// Resets room with new roo id.
	void ResetRoom(const godot::String& roomId);
	// Polls resource, returns an error code off poll. 
	// If no loader returns godot::Error::ERR_FILE_EOF.
	// If resource was polled, but resource type is invalid returns godot::Error::ERR_INVALID_DATA.
	// If resource was polled successfully returns godot::Error::ERR_FILE_EOF.
	// Otherwise returns godot::Error::OK.
	godot::Error PollResource();
	// Returns percent of resource loading, if loader is not valid will return 0;
	real_t GetPollPercent() const;
	// Complitly loads resource. Returns same error codes as PollResource.
	godot::Error WaitResource();
	// Reloads room from resource.
	bool ReloadRoom();
	// Returns true if resource is loaded.
	bool IsResLoaded() const;
	// Returns true if room is loaded.
	bool IsNodeLoaded() const;
	// Extracts Room res/node (loader resets also)
	RoomHandler ExtractRoomHandler();
	// Returns true is loader is valid.
	bool IsValid() const;

	//
	// Private members.
	//
private:
	// Actual room id.
	godot::String m_roomId;
	// Loader used for the resource.
	godot::Ref<godot::ResourceInteractiveLoader> m_loader;
	// Resource.
	godot::Ref<godot::PackedScene> m_res;
	// Instanciated room.
	shared_obj_ptr<Room> m_node;
	// Percent of loading.
	float m_loadPrecent;
	// Mutexex for all variables.
	mutable std::mutex m_resMutex;
	mutable std::mutex m_nodeMutex;
	mutable std::mutex m_loaderMutex;
	mutable std::mutex m_loadPrecentMutex;
};


} // namespace bwn

