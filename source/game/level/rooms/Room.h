#pragma once

namespace bwn 
{
class RoomAsyncLoader;

class Room : public godot::Node 
{
	friend RoomAsyncLoader;

	GODOT_CLASS(Room, godot::Node)

public:
	Room();
	~Room();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init(); 
	void _process(real_t delta);

	//
	// Public interface.
	//
public:
	const godot::String& GetId() const;

	//
	// Private methods.
	//
private:
	void SetId(const godot::String& id);

	//
	// Private members.
	//
private:
	godot::String m_id;
};

}
