#include "pch.h"

#include "GameplayState.h"

#include "level/rooms/Room.h"
#include "level/rooms/RoomHandler.h"
#include "level/actors/Character.h"

#include "global/RoomMgr.h"

#include "interface/menus/Menu.h"

using namespace bwn;

GameplayState::GameplayState()
{}
GameplayState::~GameplayState()
{}

void GameplayState::_register_methods()
{
	godot::register_method("_ready", &GameplayState::_ready);
	godot::register_method("_process", &GameplayState::_process);
	godot::register_method("_enter_tree", &GameplayState::_enter_tree);
	godot::register_method("_exit_tree", &GameplayState::_exit_tree);

	//godot::register_method("get_instance", &Gamemode::GetInstance);

	godot::register_method<bool(GameplayState::*)(const godot::String&)>("set_room", &GameplayState::SetRoom);
	godot::register_method<bool(GameplayState::*)(const godot::String&)>("add_room", &GameplayState::AddRoom);
	godot::register_method<bool(GameplayState::*)(const godot::String&)>("add_or_replace_room", &GameplayState::AddOrReplaceRoom);
	godot::register_method("remove_room", &GameplayState::RemoveRoom);
	godot::register_method("remove_rooms_except", &GameplayState::RemoveRoomsExcept);
	godot::register_method("get_room_transition", &GameplayState::GetRoomTransitionMenu);
	godot::register_method("get_main_character", &GameplayState::GetMainCharacter);

	godot::register_property("room_changing_menu", &GameplayState::m_roomChangingMenu, NodePathObject<Menu>());
	godot::register_property("main_char", &GameplayState::m_mainCharacter, NodePathObject<Character>());
}
void GameplayState::_process(real_t delta)
{}
void GameplayState::_ready()
{
	m_mainCharacter.ResetFromPath(*this);
	BWN_ASSERT_WITH_MSG(m_mainCharacter.IsObjectValid(), "Can't load character from path \"" << m_mainCharacter.GetPath().operator godot::String().ascii().get_data() << "\"");
	m_roomChangingMenu.ResetFromPath(*this);

	m_mainCharacter.SetOwner(this);
	m_roomChangingMenu.SetOwner(this);
}
void GameplayState::_enter_tree()
{
	TakeInstance();
	UpdateNeighborsLoaders();
}
void GameplayState::_exit_tree()
{	
	ResetInstance();
}

bool GameplayState::SetRoom(RoomHandlerPtr room)
{
	if (AddRoom(room))
	{
		RemoveRoomsExcept(room->GetId());
		return true;
	}

	return false;
}

bool GameplayState::AddRoom(RoomHandlerPtr room)
{
	const bool isValid = room && room->IsValid();
	BWN_ASSERT_WITH_MSG(isValid, "Trying to add invalid room.");
	if (isValid && m_rooms.find(room->GetId()) == m_rooms.end())
	{
		Room*const roomObj = room->GetRoom();
		ActivateRoom(*roomObj);
		m_rooms.emplace(room->GetId(), std::move(room));
		if (is_inside_tree())
		{
			UpdateNeighborsLoaders();
		}
		BWN_LOG("New room \"" << roomObj->get_name().ascii().get_data() << "\" added.");
		return true;
	}

	return false;
}
bool GameplayState::AddOrReplaceRoom(RoomHandlerPtr room)
{
	{
		const bool isValid = room && room->IsValid();
		BWN_ASSERT_WITH_MSG(isValid, "Trying to add invalid room.");
		if (isValid) {
			return false;
		}
	}

	auto it = m_rooms.find(room->GetId());
	const bool roomExisted = it != m_rooms.end();
	if (roomExisted)
	{
		Room*const room = it->second->GetRoom();
		if (room != nullptr)
		{
			DeactivateRoom(*room);
		}
	}

	Room*const roomObj = room->GetRoom();
	ActivateRoom(*roomObj);
	m_rooms[room->GetId()] = std::move(room);

	if (is_inside_tree())
	{
		UpdateNeighborsLoaders();
	}	

	BWN_LOG((roomExisted ? "Room \"" : "New room \"") << roomObj->get_name().ascii().get_data() << (roomExisted ? "\" replaced." : "\" added."));

	return true;
}

bool GameplayState::SetRoom(const godot::String& roomid)
{
	if (AddRoom(roomid))
	{
		RemoveRoomsExcept(roomid);
		return true;
	}

	return false;
}

bool GameplayState::AddRoom(const godot::String& roomid)
{
	return AddRoom(RoomMgr::GetInstance()->GetRoomHandler(roomid));
}
bool GameplayState::AddOrReplaceRoom(const godot::String& roomid)
{	
	return AddOrReplaceRoom(RoomMgr::GetInstance()->GetRoomHandler(roomid));
}

void GameplayState::RemoveRoom(const godot::String& roomId)
{
	auto it = m_rooms.find(roomId);
	if (it != m_rooms.end())
	{
		Room*const room = it->second->GetRoom();
		if (room != nullptr)
		{
			DeactivateRoom(*room);
		}

		if (is_inside_tree())
		{
			UpdateNeighborsLoaders();
		}
		if (room != nullptr)
		{
			BWN_LOG("Removed \"" << room->get_name().ascii().get_data() << "\" room.");
		}
		m_rooms.erase(it);
	}
}
void GameplayState::RemoveRoomsExcept(const godot::String& roomId)
{
	for (auto it = m_rooms.begin(); it != m_rooms.end();)
	{
		if (it->first != roomId)
		{
			Room*const room = it->second->GetRoom();
			if (room != nullptr)
			{
				DeactivateRoom(*room);
				BWN_LOG("Removed \"" << room->get_name().ascii().get_data() << "\" room.");
			}
			
			it = m_rooms.erase(it);
		}
		else
		{
			++it;
		}
	}
}

Menu* GameplayState::GetRoomTransitionMenu()
{
	return m_roomChangingMenu.GetObject();
}

Character* GameplayState::GetMainCharacter()
{
	return m_mainCharacter.GetObject();
}

void GameplayState::ActivateRoom(Room& room)
{
	add_child(&room);
	BWN_LOG("Room \"" << room.get_name().ascii().get_data() << "\" activated.");
}
void GameplayState::DeactivateRoom(Room& room)
{
	remove_child(&room);
	BWN_LOG("Room \"" << room.get_name().ascii().get_data() << "\" deactivated.");
}

void GameplayState::UpdateNeighborsLoaders() const
{
	std::vector<godot::String> roomIds;
	roomIds.reserve(10);

	for (const auto& roomPair : m_rooms)
	{
		if (roomPair.second->IsValid())
		{
			roomPair.second->AppendNeighbors(roomIds);
			roomIds.push_back(roomPair.second->GetId());
		}
	}

	RoomMgr::GetInstance()->RefreshRoomList(roomIds);
}

