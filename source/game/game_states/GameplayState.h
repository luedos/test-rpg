#pragma once

#include "GameState.h"
#include "utility/ManualSingleton.h"

namespace bwn
{

class Room;
class RoomHandler;
class Menu;
class Character;

class GameplayState : public GameState, public ManualSingleton<GameplayState>
{
	GODOT_SUBCLASS(GameplayState, GameState)

	//
	// Private Aliases.
	//
private:
	using RoomHandlerPtr = std::shared_ptr<RoomHandler>;

	//
	// Construction and destruction.
	//
public:
	GameplayState();
	~GameplayState();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _process(real_t delta);
	void _ready();
	void _enter_tree();
	void _exit_tree();

	//
	// Public interface.
	//
public:
	// Sets room as the only room. Returns true if room seted successfully.
	bool SetRoom(RoomHandlerPtr room);
	// Adds room to the handled of the gamemode. If room with same id allready exist does nothing. Returns true if room added successfully.
	bool AddRoom(RoomHandlerPtr room);
	// Adds room to the handled of the gamemode. If room with same id allready exist replaces it. Returns true if room added successfully.
	bool AddOrReplaceRoom(RoomHandlerPtr room);
	// Sets room as the only room. If room is not loaded to the RoomMgr, do nothing. Returns true if room seted successfully.
	bool SetRoom(const godot::String& roomid);
	// Adds room to the handled of the gamemode. If room with same id allready exist does nothing. If room is not loaded to the RoomMgr, do nothing. Returns true if room added successfully.
	bool AddRoom(const godot::String& roomid);
	// Adds room to the handled of the gamemode. If room with same id allready exist replaces it. If room is not loaded to the RoomMgr, do nothing. Returns true if room added successfully.
	bool AddOrReplaceRoom(const godot::String& roomid);
	// Removes Room from the handled.
	void RemoveRoom(const godot::String& roomId);
	// Removes all rooms except one.
	void RemoveRoomsExcept(const godot::String& roomId);
	// Returns default transition menu for room chanigng.
	Menu* GetRoomTransitionMenu();
	// Returns current main character
	Character* GetMainCharacter();

	//
	// Private method.
	//
private:
	// Activates specific room.
	void ActivateRoom(Room& room);
	// Deactivates specific room.
	void DeactivateRoom(Room& room);
	// Updates loaders for all neighbors of the current room.
	void UpdateNeighborsLoaders() const;

	//
	// Private members.
	//
private:
	// The map of all handled rooms. (those are basically weak references to the rooms)
	std::map<godot::String, RoomHandlerPtr> m_rooms;
	// Default room changing menu.
	NodePathObject<Menu> m_roomChangingMenu;
	// Main character instance.
	NodePathObject<Character> m_mainCharacter;	
};

} // namespace bwn