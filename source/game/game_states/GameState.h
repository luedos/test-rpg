#pragma once

#include "sequences/SequenceRunner.h"
#include "interface/menus/MenuStack.h"
#include "interface/menus/MenuContainer.h"

namespace bwn
{

class Menu;

class GameState : public godot::Node, private SequenceRunner
{
	GODOT_CLASS(GameState, godot::Node)

	//
	// SequenceRunner functions.
	//
public:
	using SequenceRunner::IsSequencesEmpty;
	using SequenceRunner::IsSequencesRuning;
	using SequenceRunner::IsSequencesPaused;
	using SequenceRunner::IsCurrentSequenceExclusive;
	using SequenceRunner::AddSequnce;
	using SequenceRunner::ClearSequences;
	using SequenceRunner::RemoveSequence;

	//
	// Construction and destruction.
	//
public:
	GameState();
	~GameState();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _process(real_t delta);
	void _ready();
	void _init(); 
	void _enter_tree();
	void _exit_tree();

	//
	// Public interface.
	//
public:	
	// Sets path of the default menu.
	void SetDefaultMenuPath(const godot::NodePath& path);
	// Pushes a transition menu. Transition menu is usually setuped above regular menu stack, and not affected by it.
	// Returns false if transition is allreay in process, or if menu push is failed.
	bool PushTransition(Menu* transitionMenu);
	// Pops transition menu.
	void PopTransition();
	// Returns transition menu, if one exist. 
	const Menu* GetTransition() const;
	// Returns transition menu, if one exist. 
	Menu* GetTransition();
	
	//
	// Stack menu methods.
	//
public:
	// Pushes menu into stack.
	inline bool PushMenu(Menu* menu) { return m_menuStack.PushMenu(menu); }
	// Pops menu from stack if it exist.
	inline void PopMenu(Menu* menu) { m_menuStack.PopMenu(menu); }
	// Pops top menu from stack (If no menus in the stack does nothing).
	inline void PopMenu() { m_menuStack.PopMenu(); }
	// Clears menu stack (if no menus, or only default menu is active, does nothing).
	inline void ClearMenus() { m_menuStack.ClearMenus(); }
	// Returns current menu.
	inline const Menu* GetCurrentMenu() const { return m_menuStack.GetCurrentMenu(); }
	// Returns current menu.
	inline Menu* GetCurrentMenu() { return m_menuStack.GetCurrentMenu(); }
	// Sets default menu.
	inline bool SetDefaultMenu(Menu* menu) { return m_menuStack.SetDefaultMenu(menu); }
	// Returns default menu.
	inline const Menu* GetDefaultMenu() const { return m_menuStack.GetDefaultMenu(); }
	// Returns default menu.
	inline Menu* GetDefaultMenu() { return m_menuStack.GetDefaultMenu(); }

	//
	// Private members.
	// 
private:
	// Container for the transition menu.
	MenuContainer m_transitionMenu;
	// Container for the menu stack.
	MenuStack m_menuStack;
	// Default menu, game state will return to then there is no menus in the stack. 
	// This is works only for initialization, after _ready, the only way to change it is by SetDefaultMenu
	godot::NodePath m_defaultMenuPath;
};

} // namespace bwn