#pragma once

#include "GameState.h"

namespace bwn
{

class LoadingState : public GameState
{
	GODOT_SUBCLASS(LoadingState, GameState)

	//
	// Construction and destruction.
	//
public:
	LoadingState();
	~LoadingState();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _process(real_t delta);
	void _enter_tree();

	//
	// Public interface.
	//
public:
	void SetupLoadingState(shared_obj_ptr<GameState> state);

	//
	// Private members.
	//
private:
	shared_obj_ptr<GameState> m_loadedState;
};

} // namespace bwn