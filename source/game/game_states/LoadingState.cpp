#include "pch.h"

#include "LoadingState.h"

#include "global/GameMgr.h"

using namespace bwn;

LoadingState::LoadingState()
{}
LoadingState::~LoadingState()
{}

void LoadingState::_register_methods()
{
	godot::register_method("_process", &LoadingState::_process);
}

void LoadingState::_process(real_t delta)
{
	if (IsSequencesEmpty() && m_loadedState.is_valid())
	{
		BWN_LOG("Loading is complete.");
		GameMgr::GetInstance()->SetState(std::move(m_loadedState));
	}
}

void LoadingState::SetupLoadingState(shared_obj_ptr<GameState> state)
{
	m_loadedState = state;
	BWN_ASSERT_WITH_MSG(!is_inside_tree(), "Seting target state while loading..");
}

void LoadingState::_enter_tree()
{
	BWN_ASSERT_WITH_MSG(m_loadedState.is_valid(), "Starting loading without valid target state, loading will not be complete.");
}