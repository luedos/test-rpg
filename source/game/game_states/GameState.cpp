#include "pch.h"

#include "GameState.h"
#include "interface/menus/Menu.h"

using namespace bwn;

GameState::GameState()
	: godot::Node{}
	, SequenceRunner{}
	, m_transitionMenu{ false } // not active by default
	, m_menuStack{ false } // not active by default
	, m_defaultMenuPath{}
{}

GameState::~GameState()
{
	if (!m_menuStack.IsEmpty()) {
		ClearMenus();
	}
}

void GameState::_register_methods()
{
	godot::register_method("_enter_tree", &GameState::_enter_tree);
	godot::register_method("_exit_tree", &GameState::_exit_tree);
	godot::register_method("_process", &GameState::_process);
	godot::register_method("_ready", &GameState::_ready);

	godot::register_method<bool(GameState::*)(Menu*)>("push_menu", &GameState::PushMenu);
	godot::register_method<void(GameState::*)(Menu*)>("pop_menu", &GameState::PopMenu);
	godot::register_method<void(GameState::*)(void)>("pop_current_menu", &GameState::PopMenu);
	godot::register_method<void(GameState::*)(void)>("clear_menus", &GameState::ClearMenus);
	godot::register_method<Menu*(GameState::*)()>("get_current_menu", &GameState::GetCurrentMenu);
	godot::register_method<Menu*(GameState::*)()>("get_default_menu", &GameState::GetDefaultMenu);
	godot::register_method<bool(GameState::*)(Menu*)>("set_default_menu", &GameState::SetDefaultMenu);

	godot::register_method<bool(GameState::*)(Menu*)>("push_transition", &GameState::PushTransition);
	godot::register_method<void(GameState::*)()>("pop_transition", &GameState::PopTransition);
	godot::register_method<Menu*(GameState::*)()>("get_transition", &GameState::GetTransition);
	
	godot::register_property("default_menu_path", &GameState::m_defaultMenuPath, godot::NodePath());
}

void GameState::_process(real_t delta)
{
	UpdateSequences(delta);
}
void GameState::_ready()
{
	if (!m_defaultMenuPath.is_empty()) {
		SetDefaultMenu(get_node<Menu>(m_defaultMenuPath));
	}
}
void GameState::_init()
{}
void GameState::_enter_tree()
{
	m_menuStack.SetActive(true);
	m_transitionMenu.SetActive(true);
	BWN_LOG("Game state \"" << get_name().ascii().get_data() << "\" is activated.");
}

void GameState::_exit_tree()
{	
	m_menuStack.SetActive(false);
	m_transitionMenu.SetActive(false);
	BWN_LOG("Game state \"" << get_name().ascii().get_data() << "\" is deactivated.");
}

void GameState::SetDefaultMenuPath(const godot::NodePath& path)
{
	m_defaultMenuPath = path;
	SetDefaultMenu(get_node<Menu>(m_defaultMenuPath));
}

bool GameState::PushTransition(Menu* transitionMenu)
{
	const bool success = m_transitionMenu.GetCurrentMenu() == nullptr && m_transitionMenu.PushMenu(transitionMenu);

	// TODO: Create some kind of pause mechanism here.

	return success;
}

void GameState::PopTransition()
{
	// TODO: Create some kind of pause mechanism here.
	m_transitionMenu.PopMenu();
}

const Menu* GameState::GetTransition() const
{
	return m_transitionMenu.GetCurrentMenu();
}

Menu* GameState::GetTransition()
{
	return m_transitionMenu.GetCurrentMenu();
}
