#pragma once

#include "IMenuContainer.h"

namespace bwn
{

class Menu;

class MenuStack : public IMenuContainer
{
	//
	// Private aliasing.
	//
private:
	using MenuObj = ObjectHolder<Menu>;
	using MenuContainer = std::vector<MenuObj>;

	//
	// Construction and destruction.
	//
public:
	MenuStack(bool active = true);
	~MenuStack();

	//
	// Public interface.
	//
public:
	// Pushes menu into stack.
	virtual bool PushMenu(Menu* menu) override;
	// Pops menu from stack if it exist.
	virtual void PopMenu(Menu* menu) override;
	// Pops top menu from stack (If no menus in the stack does nothing).
	virtual void PopMenu() override;
	// Clears menu stack (if no menus, or only default menu is active, does nothing).
	void ClearMenus();
	// Returns current menu.
	const Menu* GetCurrentMenu() const;
	// Returns current menu.
	Menu* GetCurrentMenu();
	// Sets default menu.
	bool SetDefaultMenu(Menu* menu);
	// Returns default menu.
	const Menu* GetDefaultMenu() const;
	// Returns default menu.
	Menu* GetDefaultMenu();
	// Sets if stack is active. If stack is inactive pushed menus will not be showed. 
	// On setting active it also hides/shows current menu.
	void SetActive(bool active);
	// Returns true if menu stack is active.
	bool IsActive() const;
	// Returns count of the menus pushed to the stack (default menu is not counted).
	std::size_t GetStackSize() const;
	// Returns true if no menus were pushed to the stack (default menu is not counted).
	bool IsEmpty() const;

	//
	// Private members.
	// 
private:
	// Menu stack.
	MenuContainer m_menus;
	// Default menu, which can't be poped.
	ObjectHolder<Menu> m_defaultMenu;
	// Is stack is active.
	bool m_active;
};

} // namespace bwn.