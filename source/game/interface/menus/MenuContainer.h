#pragma once

#include "IMenuContainer.h"

namespace bwn
{

class MenuContainer : public IMenuContainer
{

	//
	// Construction and destruction.
	//
public:
	MenuContainer(bool active = true);
	~MenuContainer();

	//
	// Public interface.
	//
public:
	// Pushes menu, returns true if menu was pushed.
	virtual bool PushMenu(Menu* menu) override;
	// Tries to pop specific menu.
	virtual void PopMenu(Menu* menu) override;
	// Pops menu.
	virtual void PopMenu() override;
	// Returns menu being hold.
	const Menu* GetCurrentMenu() const;
	// Returns menu being hold.
	Menu* GetCurrentMenu();
	// Sets if container is active. If container is inactive pushed menus will not be showed. 
	// On setting active it also hides/shows current menu.
	void SetActive(bool active);

	//
	// Private members.
	//
private:
	// The only menu being holded.
	ObjectHolder<Menu> m_menu;
	// Is container active.
	bool m_active;	
};

} // namespace bwn.