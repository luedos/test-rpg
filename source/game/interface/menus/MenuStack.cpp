#include "pch.h"

#include "MenuStack.h"
#include "Menu.h"

using namespace bwn;


MenuStack::MenuStack(bool active)
	: m_menus{}
	, m_defaultMenu{ nullptr }
	, m_active{ active }
{}
MenuStack::~MenuStack()
{}

bool MenuStack::PushMenu(Menu* menu)
{
	MenuObj menuObj{ menu };

	BWN_ASSERT_WITH_MSG(menuObj.IsObjectValid(), "Error pushing new menu, because it's invalid.");
	if (!menuObj.IsObjectValid()) {
		return false;
	}
	{
		IMenuContainer*const container = menu->GetCurrentContainer();
		if (container == this) {
			return true;
		}

		if (container != nullptr) {
			BWN_LOG_WARNING("Trying to push menu which is already pushed! It's not recomended. (menu : \"" << menu->get_name().ascii().get_data() << "\").");
		}
	}

	{
		const bool success = menu->OnPushed(*this);
		BWN_ASSERT_WITH_MSG(success, "Error pushing menu \"" << menu->get_name().ascii().get_data() << "\"");
		if (!success) {
			return false;
		}
	}

	if (m_active)
	{
		Menu*const oldMenu = GetCurrentMenu();
		
		oldMenu->SetParentMenu(menu);
		menu->SetChildMenu(oldMenu);

		if (oldMenu != nullptr)
		{
			oldMenu->OnClose();
		}
	}

	m_menus.push_back(menuObj);
	if (m_active)
	{
		menu->OnOpen();
		BWN_LOG("New menu \"" << menu->get_name().ascii().get_data() << "\" is pushed.");
	}
	else
	{
		BWN_LOG_WARNING("New menu \"" << menu->get_name().ascii().get_data() << "\" is pushed, while outside a tree.");
	}

	return true;
}

void MenuStack::PopMenu(Menu* menu)
{
	// We can't pop default menu, or unexistent one.
	if (m_menus.empty() || menu == nullptr) {
		return;
	}

	// Reversed because usually menu to pop will be top menu.
	for (MenuContainer::reverse_iterator it = m_menus.rbegin(); it != m_menus.rend(); ++it)
	{
		Menu*const stackMenu = it->GetObject();
		if (stackMenu == menu)
		{
			Menu*const parentMenu = stackMenu->GetParentMenu();
			Menu*const childMenu = stackMenu->GetChildMenu();

			if (m_active && childMenu == nullptr) {
				stackMenu->OnClose();
			}

			stackMenu->OnPoped();
			m_menus.erase(--(it.base()));

			if (parentMenu != nullptr) {
				parentMenu->SetChildMenu(childMenu);
			}

			if (childMenu != nullptr) {
				childMenu->SetParentMenu(parentMenu);
			}

			// if we have parent, but not child, this is mean parent menu is now current.
			if (m_active && childMenu == nullptr && parentMenu != nullptr) {
				parentMenu->OnOpen();
			}

			BWN_LOG("Menu \"" << stackMenu->get_name().ascii().get_data() << "\" poped, " << (GetCurrentMenu() != nullptr ? (std::string("current menu is \"") + GetCurrentMenu()->get_name().ascii().get_data() + "\".") : std::string("no current menu available.")));
	
			if (!m_active) {
				BWN_LOG_WARNING("Poping a \"" << menu->get_name().ascii().get_data() << "\" menu while outside of a tree.");
			}

			return;
		}
	}
}

void MenuStack::PopMenu()
{
	PopMenu(GetCurrentMenu());
}

void MenuStack::ClearMenus()
{
	// We can hide only top menu from the stack, not the default menu.
	if (!m_menus.empty() && m_menus.back().IsObjectValid())
	{
		m_menus.back().GetObject()->OnClose();
	}

	for (MenuContainer::reverse_iterator it = m_menus.rbegin(); it != m_menus.rend(); ++it)
	{
		Menu*const menu = it->GetObject();
		if (menu != nullptr) 
		{
			if (m_active)
			{
				menu->OnClose();
				menu->SkipTransition();
			}
			menu->OnPoped();
		}
	}

	m_menus.clear();
	Menu*const defaultMenu = m_defaultMenu.GetObject();

	if (m_active)
	{
		if (defaultMenu != nullptr)
		{
			defaultMenu->OnOpen();
			BWN_LOG("Menus stack is cleared, current menu is \"" << defaultMenu->get_name().ascii().get_data() << "\".");
		}
		else
		{
			BWN_LOG("Menu poped, no current menu is available.");
		}
	}
	else
	{
		BWN_LOG_WARNING("Menu stack is cleared, while outside the tree.");
	}
}

const Menu* MenuStack::GetCurrentMenu() const
{
	return m_menus.empty()
		? m_defaultMenu.GetObject()
		: m_menus.back().GetObject();
}

Menu* MenuStack::GetCurrentMenu()
{
	const Menu*const menu = const_cast<const MenuStack*>(this)->GetCurrentMenu();
	return const_cast<Menu*>(menu);
}

bool MenuStack::SetDefaultMenu(Menu* menu)
{
	Menu*const oldMenu = m_defaultMenu.GetObject();
	if (oldMenu == menu) {
		return true;
	}

	const bool defaultInTop = m_menus.empty();
	ObjectHolder<Menu> newMenu{ menu };

	if (newMenu.IsObjectValid())
	{
		if (!menu->OnPushed(*this))
		{
			return false;	
		}

		if (defaultInTop && m_active)
		{
			menu->OnOpen();
		}
	}

	if (oldMenu != nullptr)
	{
		if (m_active && defaultInTop)
		{
			oldMenu->OnClose();
			oldMenu->SkipTransition();
		}

		oldMenu->OnPoped();
	}

	m_defaultMenu.ResetObj(newMenu.GetObject());

	BWN_LOG((newMenu.IsObjectValid() ? (std::string("Default menu is changed to the \"") + std::string(newMenu.GetObject()->get_name().ascii().get_data()) + std::string("\".")) : std::string("Default menu is reseted.")));

	return true;
}

const Menu* MenuStack::GetDefaultMenu() const
{
	return m_defaultMenu.GetObject();
}

Menu* MenuStack::GetDefaultMenu()
{
	return m_defaultMenu.GetObject();
}

void MenuStack::SetActive(bool active)
{
	if (m_active != active)
	{
		m_active = active;
		Menu*const menu = GetCurrentMenu();
		if (menu != nullptr)
		{
			if (m_active)
			{
				menu->OnOpen();
			}
			else
			{
				menu->OnClose();
				menu->SkipTransition();
			}
		}
	}
}

bool MenuStack::IsActive() const
{
	return m_active;
}

std::size_t MenuStack::GetStackSize() const
{
	return m_menus.size();
}

bool MenuStack::IsEmpty() const
{
	return m_menus.empty();
}
