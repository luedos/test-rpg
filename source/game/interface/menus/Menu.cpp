#include "pch.h"

#include "Menu.h"

#include "IMenuContainer.h"
#include "transitions/MenuTransitions.h"
#include "transitions/MenuTransitionNode.h"

using namespace bwn;

const std::shared_ptr<OpeningMenuTransition> Menu::s_defaultOpeningTransition = std::make_shared<OpeningMenuTransition>();
const std::shared_ptr<ClosingMenuTransition> Menu::s_defaultClosingTransition = std::make_shared<ClosingMenuTransition>();

Menu::Menu()
	: m_openingTransitionNode{}
	, m_closingTransitionNode{}
	, m_container{ nullptr }
	, m_child{ nullptr }
	, m_parent{ nullptr }
	, m_state{ nullptr }
{}

Menu::~Menu()
{
	if (m_container != nullptr)
	{
		m_container->PopMenu(this);
	}
}

void Menu::_register_methods()
{
	godot::register_method("_process", &Menu::_process);
	godot::register_method("_ready", &Menu::_ready);
	godot::register_method("_exit_tree", &Menu::_exit_tree);

	godot::register_property("opening_transition", &Menu::m_openingTransitionNode, NodePathObject<MenuTransitionNode>());
	godot::register_property("closing_transition", &Menu::m_closingTransitionNode, NodePathObject<MenuTransitionNode>());
}

void Menu::_ready()
{
	// Any menues should be hidden by default.
	// BTW we can't do this in _init call, because of BIG FU FROM GODOT!!
	SetInput(false);
	SetProcess(false);
	SetVisible(false);

	m_openingTransitionNode.ResetFromPath(*this);
	m_closingTransitionNode.ResetFromPath(*this);

	m_openingTransitionNode.SetOwner(this);
	m_closingTransitionNode.SetOwner(this);
}

void Menu::_exit_tree()
{
	// This is kind of corner case, because we rarely remove menus from the tree, usually we will just turn them off.
	// Ideally we want to leave menu, if the container itself was exited, and menu was it child, but it's really tricky to check this, so it's much easier to just pop menu in any case.
	if (m_container != nullptr) {
		m_container->PopMenu(this);
	}
}

void Menu::_init()
{} 

void Menu::_process(real_t delta)
{
	if (m_state)
	{
		if (m_state->Update(*this, delta))
		{
			m_state->End(*this);
			m_state = nullptr;
		}
	}
}

void Menu::OnOpen()
{
	// We are allready showed, we dont want to be showed again.
	if (IsFullyOpened()) {
		return;
	}

	std::shared_ptr<MenuTransition> state = GetOpeningTransition();
	if (!state) {
		state = s_defaultOpeningTransition;
	}

	SetState(state);
	BWN_LOG("Menu \"" << get_name().ascii().get_data() << "\" starting to show.");
}

void Menu::OnClose()
{
	// Should do nothing if we already hidden!
	if (IsFullyClosed()) {
		return;
	}

	std::shared_ptr<MenuTransition> state = GetClosingTransition();
	if (!state) {
		state = s_defaultClosingTransition;
	}

	SetState(state);
	BWN_LOG("Menu \"" << get_name().ascii().get_data() << "\" starting to hide.");
}

bool Menu::OnPushed(IMenuContainer& container)
{
	if (m_container != nullptr) {
		m_container->PopMenu(this);
	}

	m_container = &container;

	OnPushedImpl();

	// Just for the future...
	return true;
}

void Menu::OnPoped()
{
	OnPopedImpl();

	m_container = nullptr;
	m_child = nullptr;
	m_parent = nullptr;
}

void Menu::SkipTransition()
{
	if (m_state)
	{
		m_state->Skip(*this);
		m_state->End(*this);
		m_state = nullptr;
	}
}

void Menu::SetParentMenu(Menu* menu)
{
	m_parent = menu;
}

void Menu::SetChildMenu(Menu* menu)
{
	m_child = menu;
}

const Menu* Menu::GetParentMenu() const
{
	return m_parent.GetObject();
}

const Menu* Menu::GetChildMenu() const
{
	return m_child.GetObject();
}

Menu* Menu::GetParentMenu()
{
	return m_parent.GetObject();
}

Menu* Menu::GetChildMenu()
{
	return m_child.GetObject();
}

IMenuContainer* Menu::GetCurrentContainer() const
{
	return m_container;
}

bool Menu::IsPushed() const
{
	return m_container != nullptr;
}

bool Menu::IsFullyOpened() const
{
	return !m_state && is_visible();
}

bool Menu::IsFullyClosed() const
{
	return !m_state && !is_visible();
}

bool Menu::IsHiding() const
{
	return m_state && m_state->GetType() == ETransitionType::HIDING;
}

bool Menu::IsShowing() const
{
	return m_state && m_state->GetType() == ETransitionType::SHOWING;
}

void Menu::SetState(std::shared_ptr<MenuTransition> state)
{
	SkipTransition();

	m_state = state;
	if (m_state)
	{
		state->Start(*this);
	}
}

std::shared_ptr<ClosingMenuTransition> Menu::GetClosingTransition()
{
	MenuTransitionNode*const node = m_closingTransitionNode.GetObject();
	if (node != nullptr)
	{
		return node->GetClosingTransition();
	}
	return nullptr;
}

std::shared_ptr<OpeningMenuTransition> Menu::GetOpeningTransition()
{
	MenuTransitionNode*const node = m_openingTransitionNode.GetObject();
	if (node != nullptr)
	{
		return node->GetOpeningTransition();
	}
	return nullptr;
}

void Menu::SetVisible(bool active)
{
	set_visible(active);
}

void Menu::SetProcess(bool active)
{
	set_process(active);
	set_physics_process(active);
}

void Menu::SetInput(bool active)
{
	set_process_input(active);
	set_process_unhandled_input(active);
}


