#include "pch.h"

#include "MenuContainer.h"
#include "Menu.h"

using namespace bwn;


MenuContainer::MenuContainer(bool active)
	: m_menu{ nullptr }
	, m_active{ active }
{}
MenuContainer::~MenuContainer() = default;

bool MenuContainer::PushMenu(Menu* menu)
{
	ObjectHolder<Menu> newMenu{ menu };
	if (!newMenu.IsObjectValid()) {
		return false;
	}

	{
		IMenuContainer*const container = menu->GetCurrentContainer();
		if (container == this) {
			return true;
		}

		if (container != nullptr) {
			BWN_LOG_WARNING("Trying to push menu which is already pushed! It's not recomended. (menu : \"" << menu->get_name().ascii().get_data() << "\").");
		}
	}

	if (!menu->OnPushed(*this)) {
		return false;
	}
	if (m_active) {
		menu->OnOpen();
	}

	{
		Menu*const current = m_menu.GetObject();
		if (current != nullptr)
		{
			if (m_active)
			{
				current->OnClose();
			}
			current->OnPoped();
		}
	}
	m_menu = newMenu;
	return true;
}

void MenuContainer::PopMenu(Menu* menu)
{
	Menu*const current = m_menu.GetObject();
	if (current != nullptr && current == menu) {
		MenuContainer::PopMenu();
	}	
}

void MenuContainer::PopMenu()
{
	Menu*const current = m_menu.GetObject();
	if (current != nullptr)
	{
		if (m_active)
		{
			current->OnClose();
		}

		current->OnPoped();
	}
	m_menu = nullptr;
}

const Menu* MenuContainer::GetCurrentMenu() const
{
	return m_menu.GetObject();
}

Menu* MenuContainer::GetCurrentMenu()
{
	return m_menu.GetObject();
}

void MenuContainer::SetActive(bool active)
{
	if (m_active != active)
	{
		m_active = active;
		Menu*const menu = m_menu.GetObject();
		if (menu != nullptr)
		{
			if (m_active)
			{
				menu->OnOpen();
			}
			else
			{
				menu->OnClose();
				menu->SkipTransition();
			}
		}
	}
}