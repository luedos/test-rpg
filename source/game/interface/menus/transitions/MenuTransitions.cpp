#include "pch.h"

#include "MenuTransitions.h"
#include "interface/menus/Menu.h"

using namespace bwn;

bool OpeningMenuTransition::Update(Menu& owner, real_t)
{
	Menu*const parent = owner.GetParentMenu();
	return parent == nullptr || parent->IsFullyClosed();
}
void OpeningMenuTransition::Start(Menu& owner)
{
	owner.SetVisible(true);
	owner.SetProcess(true);
}
void OpeningMenuTransition::End(Menu& owner)
{
	owner.SetInput(true);
}
void OpeningMenuTransition::Skip(Menu& owner)
{}

bool ClosingMenuTransition::Update(Menu&, real_t)
{
	return true;
}
void ClosingMenuTransition::Start(Menu& owner)
{
	owner.SetInput(false);
}
void ClosingMenuTransition::End(Menu& owner)
{
	owner.SetVisible(false);
	owner.SetProcess(false);
}
void ClosingMenuTransition::Skip(Menu& owner)
{}