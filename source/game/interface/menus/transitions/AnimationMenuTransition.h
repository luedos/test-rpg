#pragma once

#include "MenuTransitionNode.h"
#include "MenuTransitions.h"

namespace bwn
{

class AnimationMenuTransition : public MenuTransitionNode
{
	GODOT_CLASS(AnimationMenuTransition, godot::Node)

	//
	// Private classes.
	//
private:
	template<typename BaseT>
	class Transition : public BaseT
	{
		//
		// Construction and destruction.
		//
	public:
		Transition();

		//
		// Public interface.
		//
	public:
		// Sets flag if should play backwards.
		void SetIsBackward(bool backward);
		// Sets flag if should play backwards.
		bool IsBackward() const;
		// Sets animation player.
		void SetupPlayer(godot::AnimationPlayer* player);
		// Retruns true if animation player is absent or stoped playing.
		virtual bool Update(Menu& owner, real_t delta) override;
		// Start animation player.
		virtual void Start(Menu& owner) override;
		// Does nothing.
		virtual void End(Menu& owner) override;
		// Skips animation in th player.
		virtual void Skip(Menu& owner) override;

		//
		// Private members.
		//
	private:
		// Actual player.
		ObjectHolder<godot::AnimationPlayer> m_player;
		// Must play backwards.
		bool m_backward;
	};

	//
	// Public classes.
	//
	enum EAnimationFlags
	{
		NONE = 0,
		OPENING_BACKWARDS = 1 << 0,
		CLOSING_BACKWARDS = 1 << 1
	};

	//
	// Construction and destruction.
	//
public:
	AnimationMenuTransition();
	~AnimationMenuTransition();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init();
	void _ready();

	//
	// Public interface.
	//
public:
	// Sets path of the animation player.
	void SetPlayerPath(godot::NodePath path);
	// Returns path of the animation player.
	godot::NodePath GetPlayerPath() const;
	// Sets some optionflags.
	void SetFlags(int32_t flags);
	// Returns configuration flags.
	int32_t GetFlags() const;
	// Returns transition stored in object itself, not the copy.
	virtual std::shared_ptr<OpeningMenuTransition> GetOpeningTransition() const override;
	// Returns transition stored in object itself, not the copy.
	virtual std::shared_ptr<ClosingMenuTransition> GetClosingTransition() const override;

	//
	// Private members.
	//
private:
	// Used as property for simple use.
	NodePathObject<godot::AnimationPlayer> m_player;
	// transition used for closing.
	std::shared_ptr<Transition<ClosingMenuTransition>> m_closingTransition;
	// Transition used for opening.
	std::shared_ptr<Transition<OpeningMenuTransition>> m_openingTransition;
};

} // namespace bwn.