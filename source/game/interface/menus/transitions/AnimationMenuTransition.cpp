#include "pch.h"

#include "AnimationMenuTransition.h"

using namespace bwn;


template<typename BaseT>
AnimationMenuTransition::Transition<BaseT>::Transition()
	: m_player{ nullptr }
	, m_backward{ false }
{}

template<typename BaseT>
void AnimationMenuTransition::Transition<BaseT>::SetIsBackward(bool backward)
{
	m_backward = backward;
}

template<typename BaseT>
bool AnimationMenuTransition::Transition<BaseT>::IsBackward() const
{
	return m_backward;
}

template<typename BaseT>
void AnimationMenuTransition::Transition<BaseT>::SetupPlayer(godot::AnimationPlayer* player)
{
	m_player = player;
}
template<typename BaseT>
bool AnimationMenuTransition::Transition<BaseT>::Update(Menu& owner, real_t delta) 
{
	return BaseT::Update(owner, delta) && (!m_player.IsObjectValid() || m_player.GetObject()->is_playing());
}
template<typename BaseT>
void AnimationMenuTransition::Transition<BaseT>::Start(Menu& owner) 
{
	BaseT::Start(owner);
	godot::AnimationPlayer*const player = m_player.GetObject();
	if (player != nullptr)
	{
		player->play("", -0.1f, m_backward ? -1.0f : 1.0f, m_backward);
	}
}
template<typename BaseT>
void AnimationMenuTransition::Transition<BaseT>::End(Menu& owner) 
{
	BaseT::End(owner);
}
template<typename BaseT>
void AnimationMenuTransition::Transition<BaseT>::Skip(Menu& owner) 
{
	BaseT::Skip(owner);
	godot::AnimationPlayer*const player = m_player.GetObject();
	if (player != nullptr)
	{
		const bool backward = player->get_playing_speed() < 0.0f;
		player->seek(backward ? 0.0f : player->get_current_animation_length());
	}
}

AnimationMenuTransition::AnimationMenuTransition()
	: m_openingTransition{ std::make_shared<Transition<OpeningMenuTransition>>() }
	, m_closingTransition{ std::make_shared<Transition<ClosingMenuTransition>>() }
{}

AnimationMenuTransition::~AnimationMenuTransition() = default;

void AnimationMenuTransition::_register_methods()
{
	godot::register_method("_ready", &AnimationMenuTransition::_ready);

	godot::register_property("animation_player", &AnimationMenuTransition::SetPlayerPath, &AnimationMenuTransition::GetPlayerPath, godot::NodePath());

	
	godot::register_property<AnimationMenuTransition, int32_t>(
		"flags", 
		&AnimationMenuTransition::SetFlags, 
		&AnimationMenuTransition::GetFlags, 
		EAnimationFlags::NONE, 
		godot_method_rpc_mode::GODOT_METHOD_RPC_MODE_DISABLED, 
		godot_property_usage_flags::GODOT_PROPERTY_USAGE_DEFAULT, 
		godot_property_hint::GODOT_PROPERTY_HINT_FLAGS, 
		"OPENING_BACKWARDS,CLOSING_BACKWARDS");
}

void AnimationMenuTransition::_init()
{}

void AnimationMenuTransition::_ready()
{
	m_player.ResetFromPath(*this);
	m_player.SetOwner(this);
}

void AnimationMenuTransition::SetPlayerPath(godot::NodePath path)
{
	m_player.SetPath(path);
	godot::AnimationPlayer*const player = m_player.GetObject();
	m_openingTransition->SetupPlayer(player);
	m_closingTransition->SetupPlayer(player);
}

godot::NodePath AnimationMenuTransition::GetPlayerPath() const
{
	return m_player.GetPath();
}

void AnimationMenuTransition::SetFlags(int32_t flags)
{
	m_openingTransition->SetIsBackward((flags & EAnimationFlags::OPENING_BACKWARDS) != EAnimationFlags::NONE);
	m_closingTransition->SetIsBackward((flags & EAnimationFlags::CLOSING_BACKWARDS) != EAnimationFlags::NONE);
}

int32_t AnimationMenuTransition::GetFlags() const
{
	return EAnimationFlags(
		(m_openingTransition->IsBackward() * EAnimationFlags::OPENING_BACKWARDS)
		| (m_closingTransition->IsBackward() * EAnimationFlags::CLOSING_BACKWARDS));
}

std::shared_ptr<OpeningMenuTransition> AnimationMenuTransition::GetOpeningTransition() const
{
	return m_openingTransition;
}

std::shared_ptr<ClosingMenuTransition> AnimationMenuTransition::GetClosingTransition() const
{
	return m_closingTransition;
}
