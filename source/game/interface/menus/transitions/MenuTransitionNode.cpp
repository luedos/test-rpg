#include "pch.h"

#include "MenuTransitionNode.h"
#include "MenuTransitions.h"

using namespace bwn;

std::shared_ptr<OpeningMenuTransition> MenuTransitionNode::GetOpeningTransition() const
{
	return nullptr;
}

std::shared_ptr<ClosingMenuTransition> MenuTransitionNode::GetClosingTransition() const
{
	return nullptr;
}