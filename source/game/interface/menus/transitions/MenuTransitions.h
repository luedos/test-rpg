#pragma once

namespace bwn
{

class Menu;

enum class ETransitionType
{
	HIDING,
	SHOWING
};
class MenuTransition
{
	//
	// Constrution and destruction.
	//
public:
	// Constructor.
	MenuTransition(ETransitionType type)
		: m_type{ type }
	{}
	virtual ~MenuTransition() = default;
	//
	// Public interface.
	//
public:
	// Called from menu update, returns true when finished.
	virtual bool Update(Menu& owner, real_t delta) = 0;
	// Called on state changed.
	virtual void Start(Menu& owner) = 0;
	// Called then state is replaced by another.
	virtual void End(Menu& owner) = 0;
	// Skips whole transition.
	virtual void Skip(Menu& owner) = 0;
	// Getter for state type.
	ETransitionType GetType() const { return m_type; }
	//
	// Private members.
	//
private:
	ETransitionType m_type;
};
// The start of showing stage, sets process and visibility as true on the start, and sets input as true in the end.
class OpeningMenuTransition : public MenuTransition 
{ 
	//
	// Construction and destruction.
	//
public: 
	OpeningMenuTransition() 
		: MenuTransition(ETransitionType::SHOWING) 
	{} 
	//
	// Public interface.
	//
public:
	// This state doesn't do anything, so this is just returns true.
	virtual bool Update(Menu& owner, real_t delta) override;
	// Sets process and visibility as true.
	virtual void Start(Menu& owner) override;
	// Sets input as true.
	virtual void End(Menu& owner) override;
	// In this case, basically does nothing.
	virtual void Skip(Menu& owner) override;
};
// The start of hiding stage, sets input as false on the start, and sets process and visibility as false in the end.
class ClosingMenuTransition : public MenuTransition 
{ 
	//
	// Construction and destruction.
	//
public: 
	ClosingMenuTransition() 
		: MenuTransition(ETransitionType::HIDING) 
	{} 
	//
	// Public interface.
	//
public:
	// This state doesn't do anything, so this is just returns true.
	virtual bool Update(Menu& owner, real_t delta) override;
	// Sets input as false.
	virtual void Start(Menu& owner) override;
	// Sets visibility and process as false.
	virtual void End(Menu& owner) override;
	// In this case, basically does nothing.
	virtual void Skip(Menu& owner) override;
};

} // namespace bwn.