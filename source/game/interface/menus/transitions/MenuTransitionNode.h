#pragma once

namespace bwn
{

class OpeningMenuTransition;
class ClosingMenuTransition;

class MenuTransitionNode : public godot::Node
{
	GODOT_CLASS(MenuTransitionNode, godot::Node)

	//
	// Public interface.
	//
public:
	virtual std::shared_ptr<OpeningMenuTransition> GetOpeningTransition() const;
	virtual std::shared_ptr<ClosingMenuTransition> GetClosingTransition() const;
};

} // namespace bwn.