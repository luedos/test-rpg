#pragma once

namespace bwn
{

class Menu;

class IMenuContainer
{

	//
	// Public interface.
	//
public:
	// Pushes menu, returns true if menu was pushed.
	virtual bool PushMenu(Menu* menu) = 0;
	// Tries to pop specific menu.
	virtual void PopMenu(Menu* menu) = 0;
	// Pops menu.
	virtual void PopMenu() = 0;
};

} // namespace bwn.