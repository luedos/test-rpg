#pragma once

namespace bwn
{

class IMenuContainer;
class MenuTransition;
class OpeningMenuTransition;
class ClosingMenuTransition;
class MenuTransitionNode;

class Menu : public godot::CanvasItem
{
	GODOT_CLASS(Menu, godot::CanvasItem)

	//
	// Construction and destruction.
	//
public:
	Menu();
	~Menu();

	//
	// Godot methods.
	//
public:
	static void _register_methods();
	void _init();
	void _ready();
	void _exit_tree();
	void _process(real_t delta);
	
	//
	// Public interface.
	// 
public:
	// Called on menu being showed.
	void OnOpen();
	// Called on menu being hidden.
	void OnClose();
	// Signal which is called on menu pushed to the menu stack. Returns true if menu pushed successfully.
	bool OnPushed(IMenuContainer& container);	
	// Signal which is called on menu poped from the menu stack.
	void OnPoped();
	// Skips transition, if one exist.
	void SkipTransition();
	// Sets parent menu (ususally called when pushed to the stack).
	void SetParentMenu(Menu* menu);
	// Sets child menu (ususally called right before chiled menu is pushed).
	void SetChildMenu(Menu* menu);
	// Returns parent menu.
	const Menu* GetParentMenu() const;
	// Returns child menu.
	const Menu* GetChildMenu() const;
	// Returns parent menu.
	Menu* GetParentMenu();
	// Returns child menu.
	Menu* GetChildMenu();
	// Returns value of cashed stack.
	IMenuContainer* GetCurrentContainer() const;
	// Returns true if current cashed stack value is valid.
	bool IsPushed() const;
	// Returns true if state is fully opened (if it's has no transition state, and visible).
	bool IsFullyOpened() const;
	// Returns true if state is fully closed (if it's has no transition state, and invisible).
	bool IsFullyClosed() const;
	// Returns true if menu has some transition state, and this state is of hiding.
	bool IsHiding() const;
	// Returns true if menu has some transition state, and this state is of showing.
	bool IsShowing() const;
	// Sets visibility flag of the object.
	void SetVisible(bool active);
	// Sets bunch of object flags like process, visibility.
	void SetProcess(bool active);
	// Sets input object flags.
	void SetInput(bool active);

	//
	// Private methods.
	//
private:
	// Sets next state (ususally called from MenuState). New state can't be nullptr.
	void SetState(std::shared_ptr<MenuTransition> state);
	// Returns state for when menu is hiding.
	virtual std::shared_ptr<ClosingMenuTransition> GetClosingTransition();
	// Returns state for when menu is showing.
	virtual std::shared_ptr<OpeningMenuTransition> GetOpeningTransition();
	// Implementation callback of menu been pushed to the menu stack.
	virtual void OnPushedImpl() {};
	// Implementation callback of menu been poped from the menu stack.
	virtual void OnPopedImpl() {};

	//
	// Protected members.
	//
protected:
	
	//
	// Public members.
	//
public:
	static const std::shared_ptr<OpeningMenuTransition> s_defaultOpeningTransition;
	static const std::shared_ptr<ClosingMenuTransition> s_defaultClosingTransition;

	//
	// Private members.
	//
private:
	// Opening transition node, which will be used by default.
	NodePathObject<MenuTransitionNode> m_openingTransitionNode;
	// Closing transition node, which will be used by default.
	NodePathObject<MenuTransitionNode> m_closingTransitionNode;
	// Pointer to the current container.
	IMenuContainer* m_container;
	// Child menu.
	ObjectHolder<Menu> m_child;
	// Parent menu.
	ObjectHolder<Menu> m_parent;
	// Current menu state. (Default menu state is s_defaultHiddenState)
	std::shared_ptr<MenuTransition> m_state;
};

} // namespace bwn.