extends Node

export var n: String

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Simple ready from 4")

func check():
	print("Parent status is %s" % (get_parent() != null))

func process_exit_tree(tag:String):
	var parent = get_parent()
	if parent != null:
		print("[%s][%s] exiting the tree and parent (%s) tree status is %s" % [tag, name, parent.name, parent.is_inside_tree()])
	else:
		print("[%s][%s] exiting the tree and there is no parent" % [tag,name])
	

func _notification(what):
	match what:
		NOTIFICATION_UNPARENTED:
			process_exit_tree("unparented")
		NOTIFICATION_EXIT_TREE:
			process_exit_tree("notification")
		NOTIFICATION_PROCESS:
			pass
		NOTIFICATION_PHYSICS_PROCESS:
			pass
		NOTIFICATION_DRAG_BEGIN:
			pass
		NOTIFICATION_DRAG_END:
			pass
		_:
			print("[%s]Simple notification %s"%[name,str(what)])

func _exit_tree():	
	process_exit_tree("signal")

func _init():
	print("Simple init from 4 %s" % str(name))
