extends Node


onready var child = $child
var count:float
var cashed_value:bool

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	count += delta
	var value = (int(count / 3) % 2) == 1
	if value != cashed_value:
		cashed_value = value
		print("\n================================================")
		if child.get_parent() != null:
			remove_child(child)
		else:
			add_child(child)
			
		print("Finale parent child tree status: %s"% child.is_inside_tree())
		child.check()
		
		print("================================================\n")
