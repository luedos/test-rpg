extends Node

export var n: String

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Simple ready from 3 %s"%n)

func _notification(what):
	match what:
		NOTIFICATION_PROCESS:
			pass
		NOTIFICATION_PHYSICS_PROCESS:
			pass
		NOTIFICATION_DRAG_BEGIN:
			pass
		NOTIFICATION_DRAG_END:
			pass
		NOTIFICATION_POSTINITIALIZE:
			print("Simple notification postinit 3 %s"%n)
		NOTIFICATION_INSTANCED:
			print("Simple notification instanced 3 %s"%n)
		NOTIFICATION_PARENTED:
			print("Simple notification parented 3 %s"%n)
		_:
			print("Simple notification 3 %s %s"%[str(what), n])

func check_value():
	print("Simple check from 3 %s"%n)

func _init():
	print("Simple init from 3 %s"%n)
